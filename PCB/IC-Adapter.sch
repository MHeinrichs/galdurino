<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="6.6.0">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="1" fill="3" visible="no" active="no"/>
<layer number="3" name="Route3" color="4" fill="3" visible="no" active="no"/>
<layer number="14" name="Route14" color="1" fill="6" visible="no" active="no"/>
<layer number="15" name="Route15" color="4" fill="6" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="ic-package">
<description>&lt;b&gt;IC Packages an Sockets&lt;/b&gt;&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="DIL24-3">
<description>&lt;b&gt;Dual In Line Package&lt;/b&gt; 0.3 inch</description>
<wire x1="-14.986" y1="-0.635" x2="-14.986" y2="-2.794" width="0.1524" layer="21"/>
<wire x1="-14.986" y1="0.635" x2="-14.986" y2="-0.635" width="0.1524" layer="21" curve="-180"/>
<wire x1="14.986" y1="-2.794" x2="14.986" y2="2.794" width="0.1524" layer="21"/>
<wire x1="-14.986" y1="-2.794" x2="14.986" y2="-2.794" width="0.1524" layer="21"/>
<wire x1="-14.986" y1="2.794" x2="14.986" y2="2.794" width="0.1524" layer="21"/>
<wire x1="-14.986" y1="2.794" x2="-14.986" y2="0.635" width="0.1524" layer="21"/>
<pad name="1" x="-13.97" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="2" x="-11.43" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="3" x="-8.89" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="4" x="-6.35" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="5" x="-3.81" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="6" x="-1.27" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="7" x="1.27" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="8" x="3.81" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="9" x="6.35" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="10" x="8.89" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="11" x="11.43" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="12" x="13.97" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="13" x="13.97" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="14" x="11.43" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="15" x="8.89" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="16" x="6.35" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="17" x="3.81" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="18" x="1.27" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="19" x="-1.27" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="20" x="-3.81" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="21" x="-6.35" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="22" x="-8.89" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="23" x="-11.43" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="24" x="-13.97" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<text x="-15.2908" y="-2.667" size="1.778" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="-13.462" y="-0.889" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="DIL24-4">
<description>&lt;b&gt;Dual In Line Package&lt;/b&gt; 0.4 inch</description>
<wire x1="-15.367" y1="-0.635" x2="-15.367" y2="-4.064" width="0.1524" layer="21"/>
<wire x1="-15.367" y1="0.635" x2="-15.367" y2="-0.635" width="0.1524" layer="21" curve="-180"/>
<wire x1="15.367" y1="-4.064" x2="15.367" y2="4.064" width="0.1524" layer="21"/>
<wire x1="-15.367" y1="-4.064" x2="15.367" y2="-4.064" width="0.1524" layer="21"/>
<wire x1="-15.367" y1="4.064" x2="-15.367" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-15.367" y1="4.064" x2="15.367" y2="4.064" width="0.1524" layer="21"/>
<pad name="1" x="-13.97" y="-5.08" drill="0.8128" shape="long" rot="R90"/>
<pad name="2" x="-11.43" y="-5.08" drill="0.8128" shape="long" rot="R90"/>
<pad name="3" x="-8.89" y="-5.08" drill="0.8128" shape="long" rot="R90"/>
<pad name="4" x="-6.35" y="-5.08" drill="0.8128" shape="long" rot="R90"/>
<pad name="5" x="-3.81" y="-5.08" drill="0.8128" shape="long" rot="R90"/>
<pad name="6" x="-1.27" y="-5.08" drill="0.8128" shape="long" rot="R90"/>
<pad name="7" x="1.27" y="-5.08" drill="0.8128" shape="long" rot="R90"/>
<pad name="8" x="3.81" y="-5.08" drill="0.8128" shape="long" rot="R90"/>
<pad name="9" x="6.35" y="-5.08" drill="0.8128" shape="long" rot="R90"/>
<pad name="10" x="8.89" y="-5.08" drill="0.8128" shape="long" rot="R90"/>
<pad name="11" x="11.43" y="-5.08" drill="0.8128" shape="long" rot="R90"/>
<pad name="12" x="13.97" y="-5.08" drill="0.8128" shape="long" rot="R90"/>
<pad name="13" x="13.97" y="5.08" drill="0.8128" shape="long" rot="R90"/>
<pad name="14" x="11.43" y="5.08" drill="0.8128" shape="long" rot="R90"/>
<pad name="15" x="8.89" y="5.08" drill="0.8128" shape="long" rot="R90"/>
<pad name="16" x="6.35" y="5.08" drill="0.8128" shape="long" rot="R90"/>
<pad name="17" x="3.81" y="5.08" drill="0.8128" shape="long" rot="R90"/>
<pad name="18" x="1.27" y="5.08" drill="0.8128" shape="long" rot="R90"/>
<pad name="19" x="-1.27" y="5.08" drill="0.8128" shape="long" rot="R90"/>
<pad name="20" x="-3.81" y="5.08" drill="0.8128" shape="long" rot="R90"/>
<pad name="21" x="-6.35" y="5.08" drill="0.8128" shape="long" rot="R90"/>
<pad name="22" x="-8.89" y="5.08" drill="0.8128" shape="long" rot="R90"/>
<pad name="23" x="-11.43" y="5.08" drill="0.8128" shape="long" rot="R90"/>
<pad name="24" x="-13.97" y="5.08" drill="0.8128" shape="long" rot="R90"/>
<text x="-15.6464" y="-3.81" size="1.778" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="-12.7" y="-0.8382" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="DIL24-6">
<description>&lt;b&gt;Dual In Line Package&lt;/b&gt; 0.6 inch</description>
<wire x1="-15.113" y1="-1.27" x2="-15.113" y2="-6.604" width="0.1524" layer="21"/>
<wire x1="-15.113" y1="1.27" x2="-15.113" y2="-1.27" width="0.1524" layer="21" curve="-180"/>
<wire x1="15.113" y1="-6.604" x2="15.113" y2="6.604" width="0.1524" layer="21"/>
<wire x1="-15.113" y1="6.604" x2="-15.113" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-15.113" y1="6.604" x2="15.113" y2="6.604" width="0.1524" layer="21"/>
<wire x1="-15.113" y1="-6.604" x2="15.113" y2="-6.604" width="0.1524" layer="21"/>
<pad name="1" x="-13.97" y="-7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="2" x="-11.43" y="-7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="3" x="-8.89" y="-7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="4" x="-6.35" y="-7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="5" x="-3.81" y="-7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="6" x="-1.27" y="-7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="7" x="1.27" y="-7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="8" x="3.81" y="-7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="9" x="6.35" y="-7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="10" x="8.89" y="-7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="11" x="11.43" y="-7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="12" x="13.97" y="-7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="13" x="13.97" y="7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="14" x="11.43" y="7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="15" x="8.89" y="7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="16" x="6.35" y="7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="17" x="3.81" y="7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="18" x="1.27" y="7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="19" x="-1.27" y="7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="20" x="-3.81" y="7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="21" x="-6.35" y="7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="22" x="-8.89" y="7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="23" x="-11.43" y="7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="24" x="-13.97" y="7.62" drill="0.8128" shape="long" rot="R90"/>
<text x="-15.621" y="-6.35" size="1.778" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="-12.065" y="-0.889" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="DIL24-9">
<description>&lt;b&gt;Dual In Line Package&lt;/b&gt; 0.9 inch</description>
<wire x1="-16.129" y1="-1.27" x2="-16.129" y2="-10.414" width="0.1524" layer="21"/>
<wire x1="-16.129" y1="1.27" x2="-16.129" y2="-1.27" width="0.1524" layer="21" curve="-180"/>
<wire x1="16.129" y1="-10.414" x2="16.129" y2="10.414" width="0.1524" layer="21"/>
<wire x1="-16.129" y1="10.414" x2="-16.129" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-16.129" y1="10.414" x2="16.129" y2="10.414" width="0.1524" layer="21"/>
<wire x1="-16.129" y1="-10.414" x2="16.129" y2="-10.414" width="0.1524" layer="21"/>
<pad name="1" x="-13.97" y="-11.43" drill="0.8128" shape="long" rot="R90"/>
<pad name="2" x="-11.43" y="-11.43" drill="0.8128" shape="long" rot="R90"/>
<pad name="3" x="-8.89" y="-11.43" drill="0.8128" shape="long" rot="R90"/>
<pad name="4" x="-6.35" y="-11.43" drill="0.8128" shape="long" rot="R90"/>
<pad name="5" x="-3.81" y="-11.43" drill="0.8128" shape="long" rot="R90"/>
<pad name="6" x="-1.27" y="-11.43" drill="0.8128" shape="long" rot="R90"/>
<pad name="7" x="1.27" y="-11.43" drill="0.8128" shape="long" rot="R90"/>
<pad name="8" x="3.81" y="-11.43" drill="0.8128" shape="long" rot="R90"/>
<pad name="9" x="6.35" y="-11.43" drill="0.8128" shape="long" rot="R90"/>
<pad name="10" x="8.89" y="-11.43" drill="0.8128" shape="long" rot="R90"/>
<pad name="11" x="11.43" y="-11.43" drill="0.8128" shape="long" rot="R90"/>
<pad name="12" x="13.97" y="-11.43" drill="0.8128" shape="long" rot="R90"/>
<pad name="13" x="13.97" y="11.43" drill="0.8128" shape="long" rot="R90"/>
<pad name="14" x="11.43" y="11.43" drill="0.8128" shape="long" rot="R90"/>
<pad name="15" x="8.89" y="11.43" drill="0.8128" shape="long" rot="R90"/>
<pad name="16" x="6.35" y="11.43" drill="0.8128" shape="long" rot="R90"/>
<pad name="17" x="3.81" y="11.43" drill="0.8128" shape="long" rot="R90"/>
<pad name="18" x="1.27" y="11.43" drill="0.8128" shape="long" rot="R90"/>
<pad name="19" x="-1.27" y="11.43" drill="0.8128" shape="long" rot="R90"/>
<pad name="20" x="-3.81" y="11.43" drill="0.8128" shape="long" rot="R90"/>
<pad name="21" x="-6.35" y="11.43" drill="0.8128" shape="long" rot="R90"/>
<pad name="22" x="-8.89" y="11.43" drill="0.8128" shape="long" rot="R90"/>
<pad name="23" x="-11.43" y="11.43" drill="0.8128" shape="long" rot="R90"/>
<pad name="24" x="-13.97" y="11.43" drill="0.8128" shape="long" rot="R90"/>
<text x="-16.51" y="-9.906" size="1.778" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="-11.43" y="-0.635" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="PLCC28">
<description>&lt;b&gt;Plastic Leaded Chip Carrier&lt;/b&gt;&lt;p&gt;IPC standard</description>
<wire x1="6.1999" y1="6.1999" x2="-3.9649" y2="6.1999" width="0.2032" layer="51"/>
<wire x1="-6.1999" y1="3.9649" x2="-6.1999" y2="-6.1999" width="0.2032" layer="51"/>
<wire x1="-6.1999" y1="-6.1999" x2="6.1999" y2="-6.1999" width="0.2032" layer="51"/>
<wire x1="6.1999" y1="-6.1999" x2="6.1999" y2="6.1999" width="0.2032" layer="51"/>
<wire x1="-3.9649" y1="6.1999" x2="-6.1999" y2="3.9649" width="0.2032" layer="51"/>
<wire x1="-4.4729" y1="5.6919" x2="-5.6919" y2="4.4729" width="0.2032" layer="21"/>
<wire x1="6.1999" y1="6.1999" x2="4.4171" y2="6.1999" width="0.2032" layer="21"/>
<wire x1="6.1999" y1="4.4681" x2="6.1999" y2="6.1999" width="0.2032" layer="21"/>
<wire x1="6.1999" y1="-6.1999" x2="6.1999" y2="-4.4171" width="0.2032" layer="21"/>
<wire x1="4.4681" y1="-6.1999" x2="6.1999" y2="-6.1999" width="0.2032" layer="21"/>
<wire x1="-6.1999" y1="-6.1999" x2="-4.4171" y2="-6.1999" width="0.2032" layer="21"/>
<wire x1="-6.1999" y1="-4.4681" x2="-6.1999" y2="-6.1999" width="0.2032" layer="21"/>
<circle x="0" y="5.1001" radius="0.3" width="0.6096" layer="51"/>
<smd name="1" x="0" y="5.5999" dx="0.5996" dy="2.1998" layer="1"/>
<smd name="2" x="-1.27" y="5.5999" dx="0.5996" dy="2.1998" layer="1"/>
<smd name="3" x="-2.54" y="5.5999" dx="0.5996" dy="2.1998" layer="1"/>
<smd name="4" x="-3.81" y="5.5999" dx="0.5996" dy="2.1998" layer="1"/>
<smd name="5" x="-5.5999" y="3.81" dx="2.1998" dy="0.5996" layer="1"/>
<smd name="6" x="-5.5999" y="2.54" dx="2.1998" dy="0.5996" layer="1"/>
<smd name="7" x="-5.5999" y="1.27" dx="2.1998" dy="0.5996" layer="1"/>
<smd name="8" x="-5.5999" y="0" dx="2.1998" dy="0.5996" layer="1"/>
<smd name="9" x="-5.5999" y="-1.27" dx="2.1998" dy="0.5996" layer="1"/>
<smd name="10" x="-5.5999" y="-2.54" dx="2.1998" dy="0.5996" layer="1"/>
<smd name="11" x="-5.5999" y="-3.81" dx="2.1998" dy="0.5996" layer="1"/>
<smd name="12" x="-3.81" y="-5.5999" dx="0.5996" dy="2.1998" layer="1"/>
<smd name="13" x="-2.54" y="-5.5999" dx="0.5996" dy="2.1998" layer="1"/>
<smd name="14" x="-1.27" y="-5.5999" dx="0.5996" dy="2.1998" layer="1"/>
<smd name="15" x="0" y="-5.5999" dx="0.5996" dy="2.1998" layer="1"/>
<smd name="16" x="1.27" y="-5.5999" dx="0.5996" dy="2.1998" layer="1"/>
<smd name="17" x="2.54" y="-5.5999" dx="0.5996" dy="2.1998" layer="1"/>
<smd name="18" x="3.81" y="-5.5999" dx="0.5996" dy="2.1998" layer="1"/>
<smd name="19" x="5.5999" y="-3.81" dx="2.1998" dy="0.5996" layer="1"/>
<smd name="20" x="5.5999" y="-2.54" dx="2.1998" dy="0.5996" layer="1"/>
<smd name="21" x="5.5999" y="-1.27" dx="2.1998" dy="0.5996" layer="1"/>
<smd name="22" x="5.5999" y="0" dx="2.1998" dy="0.5996" layer="1"/>
<smd name="23" x="5.5999" y="1.27" dx="2.1998" dy="0.5996" layer="1"/>
<smd name="24" x="5.5999" y="2.54" dx="2.1998" dy="0.5996" layer="1"/>
<smd name="25" x="5.5999" y="3.81" dx="2.1998" dy="0.5996" layer="1"/>
<smd name="26" x="3.81" y="5.5999" dx="0.5996" dy="2.1998" layer="1"/>
<smd name="27" x="2.54" y="5.5999" dx="0.5996" dy="2.1998" layer="1"/>
<smd name="28" x="1.27" y="5.5999" dx="0.5996" dy="2.1998" layer="1"/>
<text x="-3.84" y="6.985" size="1.778" layer="25">&gt;NAME</text>
<text x="-3.81" y="-2.4051" size="1.778" layer="27">&gt;VALUE</text>
<rectangle x1="-0.2601" y1="6.2499" x2="0.2601" y2="6.7" layer="51"/>
<rectangle x1="-1.5301" y1="6.2499" x2="-1.0099" y2="6.7" layer="51"/>
<rectangle x1="-2.8001" y1="6.2499" x2="-2.2799" y2="6.7" layer="51"/>
<rectangle x1="-4.0701" y1="6.2499" x2="-3.5499" y2="6.7" layer="51"/>
<rectangle x1="-6.7" y1="3.5499" x2="-6.2499" y2="4.0701" layer="51"/>
<rectangle x1="-6.7" y1="2.2799" x2="-6.2499" y2="2.8001" layer="51"/>
<rectangle x1="-6.7" y1="1.0099" x2="-6.2499" y2="1.5301" layer="51"/>
<rectangle x1="-6.7" y1="-0.2601" x2="-6.2499" y2="0.2601" layer="51"/>
<rectangle x1="-6.7" y1="-1.5301" x2="-6.2499" y2="-1.0099" layer="51"/>
<rectangle x1="-6.7" y1="-2.8001" x2="-6.2499" y2="-2.2799" layer="51"/>
<rectangle x1="-6.7" y1="-4.0701" x2="-6.2499" y2="-3.5499" layer="51"/>
<rectangle x1="-4.0701" y1="-6.7" x2="-3.5499" y2="-6.2499" layer="51"/>
<rectangle x1="-2.8001" y1="-6.7" x2="-2.2799" y2="-6.2499" layer="51"/>
<rectangle x1="-1.5301" y1="-6.7" x2="-1.0099" y2="-6.2499" layer="51"/>
<rectangle x1="-0.2601" y1="-6.7" x2="0.2601" y2="-6.2499" layer="51"/>
<rectangle x1="1.0099" y1="-6.7" x2="1.5301" y2="-6.2499" layer="51"/>
<rectangle x1="2.2799" y1="-6.7" x2="2.8001" y2="-6.2499" layer="51"/>
<rectangle x1="3.5499" y1="-6.7" x2="4.0701" y2="-6.2499" layer="51"/>
<rectangle x1="6.2499" y1="-4.0701" x2="6.7" y2="-3.5499" layer="51"/>
<rectangle x1="6.2499" y1="-2.8001" x2="6.7" y2="-2.2799" layer="51"/>
<rectangle x1="6.2499" y1="-1.5301" x2="6.7" y2="-1.0099" layer="51"/>
<rectangle x1="6.2499" y1="-0.2601" x2="6.7" y2="0.2601" layer="51"/>
<rectangle x1="6.2499" y1="1.0099" x2="6.7" y2="1.5301" layer="51"/>
<rectangle x1="6.2499" y1="2.2799" x2="6.7" y2="2.8001" layer="51"/>
<rectangle x1="6.2499" y1="3.5499" x2="6.7" y2="4.0701" layer="51"/>
<rectangle x1="3.5499" y1="6.2499" x2="4.0701" y2="6.7" layer="51"/>
<rectangle x1="2.2799" y1="6.2499" x2="2.8001" y2="6.7" layer="51"/>
<rectangle x1="1.0099" y1="6.2499" x2="1.5301" y2="6.7" layer="51"/>
</package>
<package name="S28">
<description>&lt;b&gt;PLCC SOCKET&lt;/b&gt;</description>
<wire x1="-7.62" y1="9.144" x2="-9.144" y2="7.62" width="0.1524" layer="21"/>
<wire x1="-9.144" y1="7.62" x2="-9.144" y2="-8.509" width="0.1524" layer="21"/>
<wire x1="-5.715" y1="4.445" x2="-4.445" y2="5.715" width="0.1524" layer="21"/>
<wire x1="-6.858" y1="-7.874" x2="-4.699" y2="-5.715" width="0.1524" layer="21"/>
<wire x1="-6.858" y1="-7.874" x2="-7.874" y2="-6.858" width="0.1524" layer="21"/>
<wire x1="-7.874" y1="-6.858" x2="-5.715" y2="-4.699" width="0.1524" layer="21"/>
<wire x1="-5.715" y1="-4.699" x2="-5.715" y2="-4.445" width="0.1524" layer="21"/>
<wire x1="-4.699" y1="-5.715" x2="-4.445" y2="-5.715" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="-5.715" x2="-2.921" y2="-5.715" width="0.1524" layer="51"/>
<wire x1="-2.921" y1="-6.858" x2="-2.921" y2="-5.715" width="0.1524" layer="51"/>
<wire x1="-2.921" y1="-6.858" x2="-2.921" y2="-7.874" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="-7.874" x2="-2.159" y2="-7.874" width="0.1524" layer="21"/>
<wire x1="-2.159" y1="-7.874" x2="-2.159" y2="-6.858" width="0.1524" layer="21"/>
<wire x1="-2.159" y1="-5.715" x2="-2.159" y2="-6.858" width="0.1524" layer="51"/>
<wire x1="-2.159" y1="-5.715" x2="-1.651" y2="-5.715" width="0.1524" layer="51"/>
<wire x1="-0.889" y1="-5.715" x2="-0.381" y2="-5.715" width="0.1524" layer="51"/>
<wire x1="-0.381" y1="-6.858" x2="-0.381" y2="-5.715" width="0.1524" layer="51"/>
<wire x1="-0.381" y1="-6.858" x2="-0.381" y2="-7.874" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="-7.874" x2="0.381" y2="-7.874" width="0.1524" layer="21"/>
<wire x1="0.381" y1="-7.874" x2="0.381" y2="-6.858" width="0.1524" layer="21"/>
<wire x1="0.381" y1="-5.715" x2="0.381" y2="-6.858" width="0.1524" layer="51"/>
<wire x1="0.381" y1="-5.715" x2="0.889" y2="-5.715" width="0.1524" layer="51"/>
<wire x1="1.651" y1="-5.715" x2="2.159" y2="-5.715" width="0.1524" layer="51"/>
<wire x1="2.159" y1="-6.858" x2="2.159" y2="-5.715" width="0.1524" layer="51"/>
<wire x1="2.159" y1="-6.858" x2="2.159" y2="-7.874" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-7.874" x2="2.921" y2="-7.874" width="0.1524" layer="21"/>
<wire x1="2.921" y1="-7.874" x2="2.921" y2="-6.858" width="0.1524" layer="21"/>
<wire x1="2.921" y1="-5.715" x2="2.921" y2="-6.858" width="0.1524" layer="51"/>
<wire x1="2.921" y1="-5.715" x2="3.429" y2="-5.715" width="0.1524" layer="51"/>
<wire x1="4.445" y1="-5.715" x2="5.715" y2="-5.715" width="0.1524" layer="21"/>
<wire x1="7.874" y1="6.858" x2="5.715" y2="4.699" width="0.1524" layer="21"/>
<wire x1="7.874" y1="6.858" x2="6.858" y2="7.874" width="0.1524" layer="21"/>
<wire x1="6.858" y1="7.874" x2="4.699" y2="5.715" width="0.1524" layer="21"/>
<wire x1="5.715" y1="4.699" x2="5.715" y2="4.191" width="0.1524" layer="51"/>
<wire x1="5.715" y1="3.429" x2="5.715" y2="2.921" width="0.1524" layer="51"/>
<wire x1="6.985" y1="2.921" x2="5.715" y2="2.921" width="0.1524" layer="51"/>
<wire x1="6.985" y1="2.921" x2="7.874" y2="2.921" width="0.1524" layer="21"/>
<wire x1="7.874" y1="2.921" x2="7.874" y2="2.159" width="0.1524" layer="21"/>
<wire x1="7.874" y1="2.159" x2="6.985" y2="2.159" width="0.1524" layer="21"/>
<wire x1="5.715" y1="2.159" x2="6.985" y2="2.159" width="0.1524" layer="51"/>
<wire x1="5.715" y1="2.159" x2="5.715" y2="1.651" width="0.1524" layer="51"/>
<wire x1="5.715" y1="0.889" x2="5.715" y2="0.381" width="0.1524" layer="51"/>
<wire x1="6.858" y1="0.381" x2="5.715" y2="0.381" width="0.1524" layer="51"/>
<wire x1="6.858" y1="0.381" x2="7.874" y2="0.381" width="0.1524" layer="21"/>
<wire x1="7.874" y1="0.381" x2="7.874" y2="-0.381" width="0.1524" layer="21"/>
<wire x1="7.874" y1="-0.381" x2="6.858" y2="-0.381" width="0.1524" layer="21"/>
<wire x1="5.715" y1="-0.381" x2="6.858" y2="-0.381" width="0.1524" layer="51"/>
<wire x1="5.715" y1="-0.381" x2="5.715" y2="-0.889" width="0.1524" layer="51"/>
<wire x1="5.715" y1="-1.651" x2="5.715" y2="-2.159" width="0.1524" layer="51"/>
<wire x1="6.858" y1="-2.159" x2="5.715" y2="-2.159" width="0.1524" layer="51"/>
<wire x1="6.858" y1="-2.159" x2="7.874" y2="-2.159" width="0.1524" layer="21"/>
<wire x1="7.874" y1="-2.159" x2="7.874" y2="-2.921" width="0.1524" layer="21"/>
<wire x1="7.874" y1="-2.921" x2="6.858" y2="-2.921" width="0.1524" layer="21"/>
<wire x1="5.715" y1="-2.921" x2="6.858" y2="-2.921" width="0.1524" layer="51"/>
<wire x1="5.715" y1="-2.921" x2="5.715" y2="-3.429" width="0.1524" layer="51"/>
<wire x1="5.715" y1="-4.445" x2="5.715" y2="-5.715" width="0.1524" layer="21"/>
<wire x1="-5.715" y1="-4.445" x2="-5.715" y2="-4.191" width="0.1524" layer="51"/>
<wire x1="-5.715" y1="-3.429" x2="-5.715" y2="-2.921" width="0.1524" layer="51"/>
<wire x1="-6.985" y1="-2.921" x2="-5.715" y2="-2.921" width="0.1524" layer="51"/>
<wire x1="-6.985" y1="-2.921" x2="-7.874" y2="-2.921" width="0.1524" layer="21"/>
<wire x1="-7.874" y1="-2.921" x2="-7.874" y2="-2.159" width="0.1524" layer="21"/>
<wire x1="-7.874" y1="-2.159" x2="-6.985" y2="-2.159" width="0.1524" layer="21"/>
<wire x1="-5.715" y1="-2.159" x2="-6.985" y2="-2.159" width="0.1524" layer="51"/>
<wire x1="-5.715" y1="-2.159" x2="-5.715" y2="-1.651" width="0.1524" layer="51"/>
<wire x1="-5.715" y1="-0.889" x2="-5.715" y2="-0.381" width="0.1524" layer="51"/>
<wire x1="-6.985" y1="-0.381" x2="-5.715" y2="-0.381" width="0.1524" layer="51"/>
<wire x1="-6.985" y1="-0.381" x2="-7.874" y2="-0.381" width="0.1524" layer="21"/>
<wire x1="-7.874" y1="-0.381" x2="-7.874" y2="0.381" width="0.1524" layer="21"/>
<wire x1="-7.874" y1="0.381" x2="-6.985" y2="0.381" width="0.1524" layer="21"/>
<wire x1="-5.715" y1="0.381" x2="-6.985" y2="0.381" width="0.1524" layer="51"/>
<wire x1="-5.715" y1="0.381" x2="-5.715" y2="0.889" width="0.1524" layer="51"/>
<wire x1="-5.715" y1="1.651" x2="-5.715" y2="2.159" width="0.1524" layer="51"/>
<wire x1="-6.985" y1="2.159" x2="-5.715" y2="2.159" width="0.1524" layer="51"/>
<wire x1="-6.985" y1="2.159" x2="-7.874" y2="2.159" width="0.1524" layer="21"/>
<wire x1="-7.874" y1="2.159" x2="-7.874" y2="2.921" width="0.1524" layer="21"/>
<wire x1="-7.874" y1="2.921" x2="-6.985" y2="2.921" width="0.1524" layer="21"/>
<wire x1="-5.715" y1="2.921" x2="-6.985" y2="2.921" width="0.1524" layer="51"/>
<wire x1="-5.715" y1="2.921" x2="-5.715" y2="3.429" width="0.1524" layer="51"/>
<wire x1="-5.715" y1="4.191" x2="-5.715" y2="4.445" width="0.1524" layer="51"/>
<wire x1="4.699" y1="5.715" x2="4.191" y2="5.715" width="0.1524" layer="51"/>
<wire x1="3.429" y1="5.715" x2="2.921" y2="5.715" width="0.1524" layer="51"/>
<wire x1="2.921" y1="6.858" x2="2.921" y2="5.715" width="0.1524" layer="51"/>
<wire x1="2.921" y1="6.858" x2="2.921" y2="7.874" width="0.1524" layer="21"/>
<wire x1="2.921" y1="7.874" x2="2.159" y2="7.874" width="0.1524" layer="21"/>
<wire x1="2.159" y1="7.874" x2="2.159" y2="6.858" width="0.1524" layer="21"/>
<wire x1="2.159" y1="5.715" x2="2.159" y2="6.858" width="0.1524" layer="51"/>
<wire x1="2.159" y1="5.715" x2="1.651" y2="5.715" width="0.1524" layer="51"/>
<wire x1="0.889" y1="5.715" x2="0.381" y2="5.715" width="0.1524" layer="51"/>
<wire x1="0.381" y1="6.858" x2="0.381" y2="5.715" width="0.1524" layer="51"/>
<wire x1="0.381" y1="6.858" x2="0.381" y2="7.874" width="0.1524" layer="21"/>
<wire x1="0.381" y1="7.874" x2="-0.381" y2="7.874" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="7.874" x2="-0.381" y2="6.858" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="5.715" x2="-0.381" y2="6.858" width="0.1524" layer="51"/>
<wire x1="-0.381" y1="5.715" x2="-0.889" y2="5.715" width="0.1524" layer="51"/>
<wire x1="-1.651" y1="5.715" x2="-2.159" y2="5.715" width="0.1524" layer="51"/>
<wire x1="-2.159" y1="6.858" x2="-2.159" y2="5.715" width="0.1524" layer="51"/>
<wire x1="-2.159" y1="6.858" x2="-2.159" y2="7.874" width="0.1524" layer="21"/>
<wire x1="-2.159" y1="7.874" x2="-2.921" y2="7.874" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="7.874" x2="-2.921" y2="6.858" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="5.715" x2="-2.921" y2="6.858" width="0.1524" layer="51"/>
<wire x1="-2.921" y1="5.715" x2="-3.429" y2="5.715" width="0.1524" layer="51"/>
<wire x1="-4.191" y1="5.715" x2="-4.445" y2="5.715" width="0.1524" layer="51"/>
<wire x1="-4.191" y1="-7.366" x2="-4.191" y2="-5.715" width="0.1524" layer="51"/>
<wire x1="-4.191" y1="-5.715" x2="-4.445" y2="-5.715" width="0.1524" layer="51"/>
<wire x1="-4.191" y1="-7.366" x2="-4.191" y2="-7.874" width="0.1524" layer="21"/>
<wire x1="-4.191" y1="-7.874" x2="-3.429" y2="-7.874" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="-7.874" x2="-3.429" y2="-7.366" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="-5.715" x2="-3.429" y2="-7.366" width="0.1524" layer="51"/>
<wire x1="-1.651" y1="-7.366" x2="-1.651" y2="-5.715" width="0.1524" layer="51"/>
<wire x1="-1.651" y1="-7.366" x2="-1.651" y2="-7.874" width="0.1524" layer="21"/>
<wire x1="-1.651" y1="-7.874" x2="-0.889" y2="-7.874" width="0.1524" layer="21"/>
<wire x1="-0.889" y1="-7.874" x2="-0.889" y2="-7.366" width="0.1524" layer="21"/>
<wire x1="-0.889" y1="-5.715" x2="-0.889" y2="-7.366" width="0.1524" layer="51"/>
<wire x1="0.889" y1="-7.366" x2="0.889" y2="-5.715" width="0.1524" layer="51"/>
<wire x1="0.889" y1="-7.366" x2="0.889" y2="-7.874" width="0.1524" layer="21"/>
<wire x1="0.889" y1="-7.874" x2="1.651" y2="-7.874" width="0.1524" layer="21"/>
<wire x1="1.651" y1="-7.874" x2="1.651" y2="-7.366" width="0.1524" layer="21"/>
<wire x1="1.651" y1="-5.715" x2="1.651" y2="-7.366" width="0.1524" layer="51"/>
<wire x1="3.429" y1="-7.366" x2="3.429" y2="-5.715" width="0.1524" layer="51"/>
<wire x1="3.429" y1="-7.366" x2="3.429" y2="-7.874" width="0.1524" layer="21"/>
<wire x1="3.429" y1="-7.874" x2="4.191" y2="-7.874" width="0.1524" layer="21"/>
<wire x1="4.191" y1="-7.874" x2="4.191" y2="-7.366" width="0.1524" layer="21"/>
<wire x1="4.445" y1="-5.715" x2="4.191" y2="-5.715" width="0.1524" layer="51"/>
<wire x1="4.191" y1="-5.715" x2="4.191" y2="-7.366" width="0.1524" layer="51"/>
<wire x1="7.366" y1="4.191" x2="5.715" y2="4.191" width="0.1524" layer="51"/>
<wire x1="7.366" y1="4.191" x2="7.874" y2="4.191" width="0.1524" layer="21"/>
<wire x1="7.874" y1="4.191" x2="7.874" y2="3.429" width="0.1524" layer="21"/>
<wire x1="7.874" y1="3.429" x2="7.366" y2="3.429" width="0.1524" layer="21"/>
<wire x1="5.715" y1="3.429" x2="7.366" y2="3.429" width="0.1524" layer="51"/>
<wire x1="7.366" y1="1.651" x2="5.715" y2="1.651" width="0.1524" layer="51"/>
<wire x1="7.366" y1="1.651" x2="7.874" y2="1.651" width="0.1524" layer="21"/>
<wire x1="7.874" y1="1.651" x2="7.874" y2="0.889" width="0.1524" layer="21"/>
<wire x1="7.874" y1="0.889" x2="7.366" y2="0.889" width="0.1524" layer="21"/>
<wire x1="5.715" y1="0.889" x2="7.366" y2="0.889" width="0.1524" layer="51"/>
<wire x1="7.366" y1="-0.889" x2="5.715" y2="-0.889" width="0.1524" layer="51"/>
<wire x1="7.366" y1="-0.889" x2="7.874" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="7.874" y1="-0.889" x2="7.874" y2="-1.651" width="0.1524" layer="21"/>
<wire x1="7.874" y1="-1.651" x2="7.366" y2="-1.651" width="0.1524" layer="21"/>
<wire x1="5.715" y1="-1.651" x2="7.366" y2="-1.651" width="0.1524" layer="51"/>
<wire x1="7.366" y1="-3.429" x2="5.715" y2="-3.429" width="0.1524" layer="51"/>
<wire x1="7.366" y1="-3.429" x2="7.874" y2="-3.429" width="0.1524" layer="21"/>
<wire x1="7.874" y1="-3.429" x2="7.874" y2="-4.191" width="0.1524" layer="21"/>
<wire x1="7.874" y1="-4.191" x2="7.366" y2="-4.191" width="0.1524" layer="21"/>
<wire x1="5.715" y1="-4.445" x2="5.715" y2="-4.191" width="0.1524" layer="51"/>
<wire x1="5.715" y1="-4.191" x2="7.366" y2="-4.191" width="0.1524" layer="51"/>
<wire x1="-7.366" y1="-4.191" x2="-5.715" y2="-4.191" width="0.1524" layer="51"/>
<wire x1="-7.366" y1="-4.191" x2="-7.874" y2="-4.191" width="0.1524" layer="21"/>
<wire x1="-7.874" y1="-4.191" x2="-7.874" y2="-3.429" width="0.1524" layer="21"/>
<wire x1="-7.874" y1="-3.429" x2="-7.366" y2="-3.429" width="0.1524" layer="21"/>
<wire x1="-5.715" y1="-3.429" x2="-7.366" y2="-3.429" width="0.1524" layer="51"/>
<wire x1="-7.366" y1="-1.651" x2="-5.715" y2="-1.651" width="0.1524" layer="51"/>
<wire x1="-7.366" y1="-1.651" x2="-7.874" y2="-1.651" width="0.1524" layer="21"/>
<wire x1="-7.874" y1="-1.651" x2="-7.874" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="-7.874" y1="-0.889" x2="-7.366" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="-5.715" y1="-0.889" x2="-7.366" y2="-0.889" width="0.1524" layer="51"/>
<wire x1="-7.366" y1="0.889" x2="-5.715" y2="0.889" width="0.1524" layer="51"/>
<wire x1="-7.366" y1="0.889" x2="-7.874" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-7.874" y1="0.889" x2="-7.874" y2="1.651" width="0.1524" layer="21"/>
<wire x1="-7.874" y1="1.651" x2="-7.366" y2="1.651" width="0.1524" layer="21"/>
<wire x1="-5.715" y1="1.651" x2="-7.366" y2="1.651" width="0.1524" layer="51"/>
<wire x1="-7.366" y1="3.429" x2="-5.715" y2="3.429" width="0.1524" layer="51"/>
<wire x1="-7.366" y1="3.429" x2="-7.874" y2="3.429" width="0.1524" layer="21"/>
<wire x1="-7.874" y1="3.429" x2="-7.874" y2="4.191" width="0.1524" layer="21"/>
<wire x1="-7.874" y1="4.191" x2="-7.366" y2="4.191" width="0.1524" layer="21"/>
<wire x1="-5.715" y1="4.191" x2="-7.366" y2="4.191" width="0.1524" layer="51"/>
<wire x1="4.191" y1="7.366" x2="4.191" y2="5.715" width="0.1524" layer="51"/>
<wire x1="4.191" y1="7.366" x2="4.191" y2="7.874" width="0.1524" layer="21"/>
<wire x1="4.191" y1="7.874" x2="3.429" y2="7.874" width="0.1524" layer="21"/>
<wire x1="3.429" y1="7.874" x2="3.429" y2="7.366" width="0.1524" layer="21"/>
<wire x1="3.429" y1="5.715" x2="3.429" y2="7.366" width="0.1524" layer="51"/>
<wire x1="1.651" y1="7.366" x2="1.651" y2="5.715" width="0.1524" layer="51"/>
<wire x1="1.651" y1="7.366" x2="1.651" y2="7.874" width="0.1524" layer="21"/>
<wire x1="1.651" y1="7.874" x2="0.889" y2="7.874" width="0.1524" layer="21"/>
<wire x1="0.889" y1="7.874" x2="0.889" y2="7.366" width="0.1524" layer="21"/>
<wire x1="0.889" y1="5.715" x2="0.889" y2="7.366" width="0.1524" layer="51"/>
<wire x1="-0.889" y1="7.366" x2="-0.889" y2="5.715" width="0.1524" layer="51"/>
<wire x1="-0.889" y1="7.366" x2="-0.889" y2="7.874" width="0.1524" layer="21"/>
<wire x1="-0.889" y1="7.874" x2="-1.651" y2="7.874" width="0.1524" layer="21"/>
<wire x1="-1.651" y1="7.874" x2="-1.651" y2="7.366" width="0.1524" layer="21"/>
<wire x1="-1.651" y1="5.715" x2="-1.651" y2="7.366" width="0.1524" layer="51"/>
<wire x1="-3.429" y1="7.366" x2="-3.429" y2="5.715" width="0.1524" layer="51"/>
<wire x1="-3.429" y1="7.366" x2="-3.429" y2="7.874" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="7.874" x2="-4.191" y2="7.874" width="0.1524" layer="21"/>
<wire x1="-4.191" y1="7.874" x2="-4.191" y2="7.366" width="0.1524" layer="21"/>
<wire x1="-4.191" y1="5.715" x2="-4.191" y2="7.366" width="0.1524" layer="51"/>
<wire x1="-9.144" y1="-8.509" x2="-8.509" y2="-9.144" width="0.1524" layer="21" curve="90"/>
<wire x1="9.144" y1="-8.509" x2="9.144" y2="8.509" width="0.1524" layer="21"/>
<wire x1="8.509" y1="-9.144" x2="-8.509" y2="-9.144" width="0.1524" layer="21"/>
<wire x1="8.509" y1="-9.144" x2="9.144" y2="-8.509" width="0.1524" layer="21" curve="90"/>
<wire x1="8.509" y1="9.144" x2="-7.62" y2="9.144" width="0.1524" layer="21"/>
<wire x1="8.509" y1="9.144" x2="9.144" y2="8.509" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.27" y1="1.27" x2="1.27" y2="1.27" width="0.1524" layer="21"/>
<wire x1="1.27" y1="1.27" x2="0" y2="2.54" width="0.1524" layer="21"/>
<wire x1="0" y1="2.54" x2="-1.27" y2="1.27" width="0.1524" layer="21"/>
<circle x="-2.54" y="2.54" radius="1.27" width="0.1524" layer="51"/>
<circle x="2.54" y="2.54" radius="1.27" width="0.1524" layer="51"/>
<circle x="-2.54" y="-2.54" radius="1.27" width="0.1524" layer="51"/>
<circle x="2.54" y="-2.54" radius="1.27" width="0.1524" layer="51"/>
<pad name="1" x="1.27" y="3.81" drill="0.8128"/>
<pad name="2" x="-1.27" y="6.35" drill="0.8128" shape="octagon"/>
<pad name="3" x="-1.27" y="3.81" drill="0.8128" shape="octagon"/>
<pad name="4" x="-3.81" y="6.35" drill="0.8128" shape="octagon"/>
<pad name="5" x="-6.35" y="3.81" drill="0.8128" shape="octagon"/>
<pad name="6" x="-3.81" y="3.81" drill="0.8128" shape="octagon"/>
<pad name="7" x="-6.35" y="1.27" drill="0.8128" shape="octagon"/>
<pad name="8" x="-3.81" y="1.27" drill="0.8128" shape="octagon"/>
<pad name="9" x="-6.35" y="-1.27" drill="0.8128" shape="octagon"/>
<pad name="10" x="-3.81" y="-1.27" drill="0.8128" shape="octagon"/>
<pad name="11" x="-6.35" y="-3.81" drill="0.8128" shape="octagon"/>
<pad name="12" x="-3.81" y="-6.35" drill="0.8128" shape="octagon"/>
<pad name="13" x="-3.81" y="-3.81" drill="0.8128" shape="octagon"/>
<pad name="14" x="-1.27" y="-6.35" drill="0.8128" shape="octagon"/>
<pad name="15" x="-1.27" y="-3.81" drill="0.8128" shape="octagon"/>
<pad name="16" x="1.27" y="-6.35" drill="0.8128" shape="octagon"/>
<pad name="17" x="1.27" y="-3.81" drill="0.8128" shape="octagon"/>
<pad name="18" x="3.81" y="-6.35" drill="0.8128" shape="octagon"/>
<pad name="19" x="6.35" y="-3.81" drill="0.8128" shape="octagon"/>
<pad name="20" x="3.81" y="-3.81" drill="0.8128" shape="octagon"/>
<pad name="21" x="6.35" y="-1.27" drill="0.8128" shape="octagon"/>
<pad name="22" x="3.81" y="-1.27" drill="0.8128" shape="octagon"/>
<pad name="23" x="6.35" y="1.27" drill="0.8128" shape="octagon"/>
<pad name="24" x="3.81" y="1.27" drill="0.8128" shape="octagon"/>
<pad name="25" x="6.35" y="3.81" drill="0.8128" shape="octagon"/>
<pad name="26" x="3.81" y="6.35" drill="0.8128" shape="octagon"/>
<pad name="27" x="3.81" y="3.81" drill="0.8128" shape="octagon"/>
<pad name="28" x="1.27" y="6.35" drill="0.8128" shape="octagon"/>
<text x="-1.27" y="9.525" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<text x="-7.62" y="9.525" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="5.715" y="-8.255" size="1.27" layer="21" ratio="10">28</text>
<rectangle x1="-0.508" y1="-0.381" x2="0.508" y2="1.524" layer="21"/>
<rectangle x1="-0.889" y1="1.27" x2="0.889" y2="1.651" layer="21"/>
<rectangle x1="-0.635" y1="1.651" x2="0.635" y2="1.905" layer="21"/>
<rectangle x1="-0.381" y1="1.905" x2="0.381" y2="2.159" layer="21"/>
<rectangle x1="-0.127" y1="2.159" x2="0.127" y2="2.413" layer="21"/>
<rectangle x1="-1.143" y1="1.27" x2="-0.889" y2="1.397" layer="21"/>
<rectangle x1="-1.016" y1="1.397" x2="-0.889" y2="1.524" layer="21"/>
<rectangle x1="-0.762" y1="1.651" x2="-0.635" y2="1.778" layer="21"/>
<rectangle x1="-0.508" y1="1.905" x2="-0.381" y2="2.032" layer="21"/>
<rectangle x1="-0.254" y1="2.159" x2="-0.127" y2="2.286" layer="21"/>
<rectangle x1="0.127" y1="2.159" x2="0.254" y2="2.286" layer="21"/>
<rectangle x1="0.381" y1="1.905" x2="0.508" y2="2.032" layer="21"/>
<rectangle x1="0.635" y1="1.651" x2="0.762" y2="1.778" layer="21"/>
<rectangle x1="0.889" y1="1.27" x2="1.143" y2="1.397" layer="21"/>
<rectangle x1="0.889" y1="1.397" x2="1.016" y2="1.524" layer="21"/>
</package>
<package name="DIL20">
<description>&lt;b&gt;Dual In Line Package&lt;/b&gt;</description>
<wire x1="12.7" y1="2.921" x2="-12.7" y2="2.921" width="0.1524" layer="21"/>
<wire x1="-12.7" y1="-2.921" x2="12.7" y2="-2.921" width="0.1524" layer="21"/>
<wire x1="12.7" y1="2.921" x2="12.7" y2="-2.921" width="0.1524" layer="21"/>
<wire x1="-12.7" y1="2.921" x2="-12.7" y2="1.016" width="0.1524" layer="21"/>
<wire x1="-12.7" y1="-2.921" x2="-12.7" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-12.7" y1="1.016" x2="-12.7" y2="-1.016" width="0.1524" layer="21" curve="-180"/>
<pad name="1" x="-11.43" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="2" x="-8.89" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="7" x="3.81" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="8" x="6.35" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="3" x="-6.35" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="4" x="-3.81" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="6" x="1.27" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="5" x="-1.27" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="9" x="8.89" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="10" x="11.43" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="11" x="11.43" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="12" x="8.89" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="13" x="6.35" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="14" x="3.81" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="15" x="1.27" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="16" x="-1.27" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="17" x="-3.81" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="18" x="-6.35" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="19" x="-8.89" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="20" x="-11.43" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<text x="-13.081" y="-3.048" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="-9.779" y="-0.381" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="SOCKET-20">
<description>&lt;b&gt;Dual In Line Socket&lt;/b&gt;</description>
<wire x1="12.7" y1="5.08" x2="-12.7" y2="5.08" width="0.1524" layer="21"/>
<wire x1="-12.7" y1="-5.08" x2="12.7" y2="-5.08" width="0.1524" layer="21"/>
<wire x1="12.7" y1="5.08" x2="12.7" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-12.7" y1="5.08" x2="-12.7" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-12.7" y1="-5.08" x2="-12.7" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-12.7" y1="1.016" x2="-12.7" y2="-1.016" width="0.1524" layer="21" curve="-180"/>
<wire x1="-12.7" y1="2.54" x2="12.7" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-12.7" y1="2.54" x2="-12.7" y2="1.016" width="0.1524" layer="21"/>
<wire x1="12.7" y1="2.54" x2="12.7" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-12.7" y1="-2.54" x2="12.7" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-12.7" y1="-2.54" x2="-12.7" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="12.7" y1="-2.54" x2="12.7" y2="-5.08" width="0.1524" layer="21"/>
<pad name="1" x="-11.43" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="2" x="-8.89" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="5" x="-1.27" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="6" x="1.27" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="3" x="-6.35" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="4" x="-3.81" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="7" x="3.81" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="8" x="6.35" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="9" x="8.89" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="10" x="11.43" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="11" x="11.43" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="12" x="8.89" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="13" x="6.35" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="14" x="3.81" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="15" x="1.27" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="16" x="-1.27" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="17" x="-3.81" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="18" x="-6.35" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="19" x="-8.89" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="20" x="-11.43" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<text x="-6.477" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<text x="-12.954" y="-4.953" size="1.27" layer="25" ratio="10" rot="R90">&gt;NAME</text>
</package>
<package name="PLCC20">
<description>&lt;b&gt;Plastic Leaded Chip Carrier&lt;/b&gt;&lt;p&gt;IPC standard</description>
<wire x1="4.8999" y1="4.8999" x2="-2.665" y2="4.8999" width="0.2032" layer="51"/>
<wire x1="-4.8999" y1="2.665" x2="-4.8999" y2="-4.8999" width="0.2032" layer="51"/>
<wire x1="-4.8999" y1="-4.8999" x2="4.8999" y2="-4.8999" width="0.2032" layer="51"/>
<wire x1="4.8999" y1="-4.8999" x2="4.8999" y2="4.8999" width="0.2032" layer="51"/>
<wire x1="-2.665" y1="4.8999" x2="-4.8999" y2="2.665" width="0.2032" layer="51"/>
<wire x1="-3.173" y1="4.3919" x2="-4.3919" y2="3.173" width="0.2032" layer="21"/>
<wire x1="-4.8999" y1="-3.431" x2="-4.8999" y2="-4.8999" width="0.2032" layer="21"/>
<wire x1="-4.8999" y1="-4.8999" x2="-3.2281" y2="-4.8999" width="0.2032" layer="21"/>
<wire x1="3.431" y1="-4.8999" x2="4.8999" y2="-4.8999" width="0.2032" layer="21"/>
<wire x1="4.8999" y1="-4.8999" x2="4.8999" y2="-3.2281" width="0.2032" layer="21"/>
<wire x1="4.8999" y1="3.431" x2="4.8999" y2="4.8999" width="0.2032" layer="21"/>
<wire x1="4.8999" y1="4.8999" x2="3.2281" y2="4.8999" width="0.2032" layer="21"/>
<circle x="0" y="3.8001" radius="0.3" width="0.6096" layer="51"/>
<smd name="1" x="0" y="4.3" dx="0.5996" dy="2.1998" layer="1"/>
<smd name="2" x="-1.27" y="4.3" dx="0.5996" dy="2.1998" layer="1"/>
<smd name="3" x="-2.54" y="4.3" dx="0.5996" dy="2.1998" layer="1"/>
<smd name="4" x="-4.3" y="2.54" dx="2.1998" dy="0.5996" layer="1"/>
<smd name="5" x="-4.3" y="1.27" dx="2.1998" dy="0.5996" layer="1"/>
<smd name="6" x="-4.3" y="0" dx="2.1998" dy="0.5996" layer="1"/>
<smd name="7" x="-4.3" y="-1.27" dx="2.1998" dy="0.5996" layer="1"/>
<smd name="8" x="-4.3" y="-2.54" dx="2.1998" dy="0.5996" layer="1"/>
<smd name="9" x="-2.54" y="-4.3" dx="0.5996" dy="2.1998" layer="1"/>
<smd name="10" x="-1.27" y="-4.3" dx="0.5996" dy="2.1998" layer="1"/>
<smd name="11" x="0" y="-4.3" dx="0.5996" dy="2.1998" layer="1"/>
<smd name="12" x="1.27" y="-4.3" dx="0.5996" dy="2.1998" layer="1"/>
<smd name="13" x="2.54" y="-4.3" dx="0.5996" dy="2.1998" layer="1"/>
<smd name="14" x="4.3" y="-2.54" dx="2.1998" dy="0.5996" layer="1"/>
<smd name="15" x="4.3" y="-1.27" dx="2.1998" dy="0.5996" layer="1"/>
<smd name="16" x="4.3" y="0" dx="2.1998" dy="0.5996" layer="1"/>
<smd name="17" x="4.3" y="1.27" dx="2.1998" dy="0.5996" layer="1"/>
<smd name="18" x="4.3" y="2.54" dx="2.1998" dy="0.5996" layer="1"/>
<smd name="19" x="2.54" y="4.3" dx="0.5996" dy="2.1998" layer="1"/>
<smd name="20" x="1.27" y="4.3" dx="0.5996" dy="2.1998" layer="1"/>
<text x="-3.81" y="5.715" size="1.778" layer="25">&gt;NAME</text>
<text x="-4.445" y="-7.4851" size="1.778" layer="27">&gt;VALUE</text>
<rectangle x1="-0.2601" y1="4.95" x2="0.2601" y2="5.4" layer="51"/>
<rectangle x1="-1.5301" y1="4.95" x2="-1.0099" y2="5.4" layer="51"/>
<rectangle x1="-2.8001" y1="4.95" x2="-2.2799" y2="5.4" layer="51"/>
<rectangle x1="-5.4" y1="2.2799" x2="-4.95" y2="2.8001" layer="51"/>
<rectangle x1="-5.4" y1="1.0099" x2="-4.95" y2="1.5301" layer="51"/>
<rectangle x1="-5.4" y1="-0.2601" x2="-4.95" y2="0.2601" layer="51"/>
<rectangle x1="-5.4" y1="-1.5301" x2="-4.95" y2="-1.0099" layer="51"/>
<rectangle x1="-5.4" y1="-2.8001" x2="-4.95" y2="-2.2799" layer="51"/>
<rectangle x1="-2.8001" y1="-5.4" x2="-2.2799" y2="-4.95" layer="51"/>
<rectangle x1="-1.5301" y1="-5.4" x2="-1.0099" y2="-4.95" layer="51"/>
<rectangle x1="-0.2601" y1="-5.4" x2="0.2601" y2="-4.95" layer="51"/>
<rectangle x1="1.0099" y1="-5.4" x2="1.5301" y2="-4.95" layer="51"/>
<rectangle x1="2.2799" y1="-5.4" x2="2.8001" y2="-4.95" layer="51"/>
<rectangle x1="4.95" y1="-2.8001" x2="5.4" y2="-2.2799" layer="51"/>
<rectangle x1="4.95" y1="-1.5301" x2="5.4" y2="-1.0099" layer="51"/>
<rectangle x1="4.95" y1="-0.2601" x2="5.4" y2="0.2601" layer="51"/>
<rectangle x1="4.95" y1="1.0099" x2="5.4" y2="1.5301" layer="51"/>
<rectangle x1="4.95" y1="2.2799" x2="5.4" y2="2.8001" layer="51"/>
<rectangle x1="2.2799" y1="4.95" x2="2.8001" y2="5.4" layer="51"/>
<rectangle x1="1.0099" y1="4.95" x2="1.5301" y2="5.4" layer="51"/>
</package>
<package name="S20">
<description>&lt;b&gt;PLCC SOCKET&lt;/b&gt;</description>
<wire x1="-6.35" y1="7.874" x2="-7.874" y2="6.35" width="0.1524" layer="21"/>
<wire x1="-7.874" y1="6.35" x2="-7.874" y2="-7.239" width="0.1524" layer="21"/>
<wire x1="-4.445" y1="3.175" x2="-3.175" y2="4.445" width="0.1524" layer="21"/>
<wire x1="-5.588" y1="-6.604" x2="-3.429" y2="-4.445" width="0.1524" layer="21"/>
<wire x1="-5.588" y1="-6.604" x2="-6.604" y2="-5.588" width="0.1524" layer="21"/>
<wire x1="-6.604" y1="-5.588" x2="-4.445" y2="-3.429" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="-4.445" x2="-3.175" y2="-4.445" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="-6.096" x2="-2.921" y2="-4.445" width="0.1524" layer="51"/>
<wire x1="-2.921" y1="-4.445" x2="-3.175" y2="-4.445" width="0.1524" layer="51"/>
<wire x1="-2.921" y1="-6.096" x2="-2.921" y2="-6.604" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="-6.604" x2="-2.159" y2="-6.604" width="0.1524" layer="21"/>
<wire x1="-2.159" y1="-6.604" x2="-2.159" y2="-6.096" width="0.1524" layer="21"/>
<wire x1="-2.159" y1="-4.445" x2="-2.159" y2="-6.096" width="0.1524" layer="51"/>
<wire x1="-2.159" y1="-4.445" x2="-1.651" y2="-4.445" width="0.1524" layer="51"/>
<wire x1="-1.651" y1="-4.445" x2="-1.651" y2="-5.588" width="0.1524" layer="51"/>
<wire x1="-1.651" y1="-6.604" x2="-1.651" y2="-5.588" width="0.1524" layer="21"/>
<wire x1="-1.651" y1="-6.604" x2="-0.889" y2="-6.604" width="0.1524" layer="21"/>
<wire x1="-0.889" y1="-5.588" x2="-0.889" y2="-6.604" width="0.1524" layer="21"/>
<wire x1="-0.889" y1="-5.588" x2="-0.889" y2="-4.445" width="0.1524" layer="51"/>
<wire x1="-0.889" y1="-4.445" x2="-0.381" y2="-4.445" width="0.1524" layer="51"/>
<wire x1="-0.381" y1="-6.096" x2="-0.381" y2="-4.445" width="0.1524" layer="51"/>
<wire x1="-0.381" y1="-6.096" x2="-0.381" y2="-6.604" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="-6.604" x2="0.381" y2="-6.604" width="0.1524" layer="21"/>
<wire x1="0.381" y1="-6.604" x2="0.381" y2="-6.096" width="0.1524" layer="21"/>
<wire x1="0.381" y1="-4.445" x2="0.381" y2="-6.096" width="0.1524" layer="51"/>
<wire x1="0.381" y1="-4.445" x2="0.889" y2="-4.445" width="0.1524" layer="51"/>
<wire x1="0.889" y1="-4.445" x2="0.889" y2="-5.588" width="0.1524" layer="51"/>
<wire x1="0.889" y1="-6.604" x2="0.889" y2="-5.588" width="0.1524" layer="21"/>
<wire x1="0.889" y1="-6.604" x2="1.651" y2="-6.604" width="0.1524" layer="21"/>
<wire x1="1.651" y1="-5.588" x2="1.651" y2="-6.604" width="0.1524" layer="21"/>
<wire x1="1.651" y1="-5.588" x2="1.651" y2="-4.445" width="0.1524" layer="51"/>
<wire x1="1.651" y1="-4.445" x2="2.159" y2="-4.445" width="0.1524" layer="51"/>
<wire x1="2.159" y1="-6.096" x2="2.159" y2="-4.445" width="0.1524" layer="51"/>
<wire x1="2.159" y1="-6.096" x2="2.159" y2="-6.604" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-6.604" x2="2.921" y2="-6.604" width="0.1524" layer="21"/>
<wire x1="2.921" y1="-6.604" x2="2.921" y2="-6.096" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-4.445" x2="2.921" y2="-4.445" width="0.1524" layer="51"/>
<wire x1="2.921" y1="-4.445" x2="2.921" y2="-6.096" width="0.1524" layer="51"/>
<wire x1="3.175" y1="-4.445" x2="4.445" y2="-4.445" width="0.1524" layer="21"/>
<wire x1="6.604" y1="5.588" x2="4.445" y2="3.429" width="0.1524" layer="21"/>
<wire x1="6.604" y1="5.588" x2="5.588" y2="6.604" width="0.1524" layer="21"/>
<wire x1="5.588" y1="6.604" x2="3.429" y2="4.445" width="0.1524" layer="21"/>
<wire x1="4.445" y1="3.429" x2="4.445" y2="3.175" width="0.1524" layer="21"/>
<wire x1="6.096" y1="2.921" x2="4.445" y2="2.921" width="0.1524" layer="51"/>
<wire x1="4.445" y1="2.921" x2="4.445" y2="3.175" width="0.1524" layer="51"/>
<wire x1="6.096" y1="2.921" x2="6.604" y2="2.921" width="0.1524" layer="21"/>
<wire x1="6.604" y1="2.921" x2="6.604" y2="2.159" width="0.1524" layer="21"/>
<wire x1="6.604" y1="2.159" x2="6.096" y2="2.159" width="0.1524" layer="21"/>
<wire x1="4.445" y1="2.159" x2="6.096" y2="2.159" width="0.1524" layer="51"/>
<wire x1="4.445" y1="2.159" x2="4.445" y2="1.651" width="0.1524" layer="51"/>
<wire x1="4.445" y1="1.651" x2="5.588" y2="1.651" width="0.1524" layer="51"/>
<wire x1="6.604" y1="1.651" x2="5.588" y2="1.651" width="0.1524" layer="21"/>
<wire x1="6.604" y1="1.651" x2="6.604" y2="0.889" width="0.1524" layer="21"/>
<wire x1="5.588" y1="0.889" x2="6.604" y2="0.889" width="0.1524" layer="21"/>
<wire x1="5.588" y1="0.889" x2="4.445" y2="0.889" width="0.1524" layer="51"/>
<wire x1="4.445" y1="0.889" x2="4.445" y2="0.381" width="0.1524" layer="51"/>
<wire x1="6.096" y1="0.381" x2="4.445" y2="0.381" width="0.1524" layer="51"/>
<wire x1="6.096" y1="0.381" x2="6.604" y2="0.381" width="0.1524" layer="21"/>
<wire x1="6.604" y1="0.381" x2="6.604" y2="-0.381" width="0.1524" layer="21"/>
<wire x1="6.604" y1="-0.381" x2="6.096" y2="-0.381" width="0.1524" layer="21"/>
<wire x1="4.445" y1="-0.381" x2="6.096" y2="-0.381" width="0.1524" layer="51"/>
<wire x1="4.445" y1="-0.381" x2="4.445" y2="-0.889" width="0.1524" layer="51"/>
<wire x1="4.445" y1="-0.889" x2="5.588" y2="-0.889" width="0.1524" layer="51"/>
<wire x1="6.604" y1="-0.889" x2="5.588" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="6.604" y1="-0.889" x2="6.604" y2="-1.651" width="0.1524" layer="21"/>
<wire x1="5.588" y1="-1.651" x2="6.604" y2="-1.651" width="0.1524" layer="21"/>
<wire x1="5.588" y1="-1.651" x2="4.445" y2="-1.651" width="0.1524" layer="51"/>
<wire x1="4.445" y1="-1.651" x2="4.445" y2="-2.159" width="0.1524" layer="51"/>
<wire x1="6.096" y1="-2.159" x2="4.445" y2="-2.159" width="0.1524" layer="51"/>
<wire x1="6.096" y1="-2.159" x2="6.604" y2="-2.159" width="0.1524" layer="21"/>
<wire x1="6.604" y1="-2.159" x2="6.604" y2="-2.921" width="0.1524" layer="21"/>
<wire x1="6.604" y1="-2.921" x2="6.096" y2="-2.921" width="0.1524" layer="21"/>
<wire x1="4.445" y1="-3.175" x2="4.445" y2="-2.921" width="0.1524" layer="51"/>
<wire x1="4.445" y1="-2.921" x2="6.096" y2="-2.921" width="0.1524" layer="51"/>
<wire x1="4.445" y1="-3.175" x2="4.445" y2="-4.445" width="0.1524" layer="21"/>
<wire x1="-4.445" y1="-3.429" x2="-4.445" y2="-3.175" width="0.1524" layer="21"/>
<wire x1="-6.096" y1="-2.921" x2="-4.445" y2="-2.921" width="0.1524" layer="51"/>
<wire x1="-4.445" y1="-2.921" x2="-4.445" y2="-3.175" width="0.1524" layer="51"/>
<wire x1="-6.096" y1="-2.921" x2="-6.604" y2="-2.921" width="0.1524" layer="21"/>
<wire x1="-6.604" y1="-2.921" x2="-6.604" y2="-2.159" width="0.1524" layer="21"/>
<wire x1="-6.604" y1="-2.159" x2="-6.096" y2="-2.159" width="0.1524" layer="21"/>
<wire x1="-4.445" y1="-2.159" x2="-6.096" y2="-2.159" width="0.1524" layer="51"/>
<wire x1="-4.445" y1="-2.159" x2="-4.445" y2="-1.651" width="0.1524" layer="51"/>
<wire x1="-4.445" y1="-1.651" x2="-5.588" y2="-1.651" width="0.1524" layer="51"/>
<wire x1="-6.604" y1="-1.651" x2="-5.588" y2="-1.651" width="0.1524" layer="21"/>
<wire x1="-6.604" y1="-1.651" x2="-6.604" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="-5.588" y1="-0.889" x2="-6.604" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="-5.588" y1="-0.889" x2="-4.445" y2="-0.889" width="0.1524" layer="51"/>
<wire x1="-4.445" y1="-0.889" x2="-4.445" y2="-0.381" width="0.1524" layer="51"/>
<wire x1="-6.096" y1="-0.381" x2="-4.445" y2="-0.381" width="0.1524" layer="51"/>
<wire x1="-6.096" y1="-0.381" x2="-6.604" y2="-0.381" width="0.1524" layer="21"/>
<wire x1="-6.604" y1="-0.381" x2="-6.604" y2="0.381" width="0.1524" layer="21"/>
<wire x1="-6.604" y1="0.381" x2="-6.096" y2="0.381" width="0.1524" layer="21"/>
<wire x1="-4.445" y1="0.381" x2="-6.096" y2="0.381" width="0.1524" layer="51"/>
<wire x1="-4.445" y1="0.381" x2="-4.445" y2="0.889" width="0.1524" layer="51"/>
<wire x1="-4.445" y1="0.889" x2="-5.588" y2="0.889" width="0.1524" layer="51"/>
<wire x1="-6.604" y1="0.889" x2="-5.588" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-6.604" y1="0.889" x2="-6.604" y2="1.651" width="0.1524" layer="21"/>
<wire x1="-5.588" y1="1.651" x2="-6.604" y2="1.651" width="0.1524" layer="21"/>
<wire x1="-5.588" y1="1.651" x2="-4.445" y2="1.651" width="0.1524" layer="51"/>
<wire x1="-4.445" y1="1.651" x2="-4.445" y2="2.159" width="0.1524" layer="51"/>
<wire x1="-6.096" y1="2.159" x2="-4.445" y2="2.159" width="0.1524" layer="51"/>
<wire x1="-6.096" y1="2.159" x2="-6.604" y2="2.159" width="0.1524" layer="21"/>
<wire x1="-6.604" y1="2.159" x2="-6.604" y2="2.921" width="0.1524" layer="21"/>
<wire x1="-6.604" y1="2.921" x2="-6.096" y2="2.921" width="0.1524" layer="21"/>
<wire x1="-4.445" y1="2.921" x2="-6.096" y2="2.921" width="0.1524" layer="51"/>
<wire x1="-4.445" y1="2.921" x2="-4.445" y2="3.175" width="0.1524" layer="51"/>
<wire x1="3.429" y1="4.445" x2="3.175" y2="4.445" width="0.1524" layer="21"/>
<wire x1="2.921" y1="6.096" x2="2.921" y2="4.445" width="0.1524" layer="51"/>
<wire x1="2.921" y1="4.445" x2="3.175" y2="4.445" width="0.1524" layer="51"/>
<wire x1="2.921" y1="6.096" x2="2.921" y2="6.604" width="0.1524" layer="21"/>
<wire x1="2.921" y1="6.604" x2="2.159" y2="6.604" width="0.1524" layer="21"/>
<wire x1="2.159" y1="6.604" x2="2.159" y2="6.096" width="0.1524" layer="21"/>
<wire x1="2.159" y1="4.445" x2="2.159" y2="6.096" width="0.1524" layer="51"/>
<wire x1="2.159" y1="4.445" x2="1.651" y2="4.445" width="0.1524" layer="51"/>
<wire x1="1.651" y1="4.445" x2="1.651" y2="5.588" width="0.1524" layer="51"/>
<wire x1="1.651" y1="6.604" x2="1.651" y2="5.588" width="0.1524" layer="21"/>
<wire x1="1.651" y1="6.604" x2="0.889" y2="6.604" width="0.1524" layer="21"/>
<wire x1="0.889" y1="5.588" x2="0.889" y2="6.604" width="0.1524" layer="21"/>
<wire x1="0.889" y1="5.588" x2="0.889" y2="4.445" width="0.1524" layer="51"/>
<wire x1="0.889" y1="4.445" x2="0.381" y2="4.445" width="0.1524" layer="51"/>
<wire x1="0.381" y1="6.096" x2="0.381" y2="4.445" width="0.1524" layer="51"/>
<wire x1="0.381" y1="6.096" x2="0.381" y2="6.604" width="0.1524" layer="21"/>
<wire x1="0.381" y1="6.604" x2="-0.381" y2="6.604" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="6.604" x2="-0.381" y2="6.096" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="4.445" x2="-0.381" y2="6.096" width="0.1524" layer="51"/>
<wire x1="-0.381" y1="4.445" x2="-0.889" y2="4.445" width="0.1524" layer="51"/>
<wire x1="-0.889" y1="4.445" x2="-0.889" y2="5.588" width="0.1524" layer="51"/>
<wire x1="-0.889" y1="6.604" x2="-0.889" y2="5.588" width="0.1524" layer="21"/>
<wire x1="-0.889" y1="6.604" x2="-1.651" y2="6.604" width="0.1524" layer="21"/>
<wire x1="-1.651" y1="5.588" x2="-1.651" y2="6.604" width="0.1524" layer="21"/>
<wire x1="-1.651" y1="5.588" x2="-1.651" y2="4.445" width="0.1524" layer="51"/>
<wire x1="-1.651" y1="4.445" x2="-2.159" y2="4.445" width="0.1524" layer="51"/>
<wire x1="-2.159" y1="6.096" x2="-2.159" y2="4.445" width="0.1524" layer="51"/>
<wire x1="-2.159" y1="6.096" x2="-2.159" y2="6.604" width="0.1524" layer="21"/>
<wire x1="-2.159" y1="6.604" x2="-2.921" y2="6.604" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="6.604" x2="-2.921" y2="6.096" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="4.445" x2="-2.921" y2="6.096" width="0.1524" layer="51"/>
<wire x1="-2.921" y1="4.445" x2="-3.175" y2="4.445" width="0.1524" layer="51"/>
<wire x1="-7.874" y1="-7.239" x2="-7.239" y2="-7.874" width="0.1524" layer="21" curve="90"/>
<wire x1="-7.239" y1="-7.874" x2="7.239" y2="-7.874" width="0.1524" layer="21"/>
<wire x1="7.239" y1="-7.874" x2="7.874" y2="-7.239" width="0.1524" layer="21" curve="90"/>
<wire x1="7.239" y1="7.874" x2="-6.35" y2="7.874" width="0.1524" layer="21"/>
<wire x1="7.874" y1="7.239" x2="7.874" y2="-7.239" width="0.1524" layer="21"/>
<wire x1="7.239" y1="7.874" x2="7.874" y2="7.239" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.27" y1="0.254" x2="1.27" y2="0.254" width="0.1524" layer="21"/>
<wire x1="1.27" y1="0.254" x2="0" y2="1.524" width="0.1524" layer="21"/>
<wire x1="0" y1="1.524" x2="-1.27" y2="0.254" width="0.1524" layer="21"/>
<pad name="1" x="0" y="5.08" drill="0.8128"/>
<pad name="2" x="0" y="2.54" drill="0.8128" shape="octagon"/>
<pad name="3" x="-2.54" y="5.08" drill="0.8128" shape="octagon"/>
<pad name="4" x="-5.08" y="2.54" drill="0.8128" shape="octagon"/>
<pad name="5" x="-2.54" y="2.54" drill="0.8128" shape="octagon"/>
<pad name="6" x="-5.08" y="0" drill="0.8128" shape="octagon"/>
<pad name="7" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="8" x="-5.08" y="-2.54" drill="0.8128" shape="octagon"/>
<pad name="9" x="-2.54" y="-5.08" drill="0.8128" shape="octagon"/>
<pad name="10" x="-2.54" y="-2.54" drill="0.8128" shape="octagon"/>
<pad name="11" x="0" y="-5.08" drill="0.8128" shape="octagon"/>
<pad name="12" x="0" y="-2.54" drill="0.8128" shape="octagon"/>
<pad name="13" x="2.54" y="-5.08" drill="0.8128" shape="octagon"/>
<pad name="14" x="5.08" y="-2.54" drill="0.8128" shape="octagon"/>
<pad name="15" x="2.54" y="-2.54" drill="0.8128" shape="octagon"/>
<pad name="16" x="5.08" y="0" drill="0.8128" shape="octagon"/>
<pad name="17" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="18" x="5.08" y="2.54" drill="0.8128" shape="octagon"/>
<pad name="19" x="2.54" y="5.08" drill="0.8128" shape="octagon"/>
<pad name="20" x="2.54" y="2.54" drill="0.8128" shape="octagon"/>
<text x="0" y="8.255" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<text x="-6.35" y="8.255" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="4.445" y="-6.985" size="1.27" layer="21" ratio="10">20</text>
<rectangle x1="-0.508" y1="-1.397" x2="0.508" y2="0.508" layer="21"/>
<rectangle x1="-0.889" y1="0.254" x2="0.889" y2="0.635" layer="21"/>
<rectangle x1="-0.635" y1="0.635" x2="0.635" y2="0.889" layer="21"/>
<rectangle x1="-0.381" y1="0.889" x2="0.381" y2="1.143" layer="21"/>
<rectangle x1="-0.127" y1="1.143" x2="0.127" y2="1.397" layer="21"/>
<rectangle x1="-1.143" y1="0.254" x2="-0.889" y2="0.381" layer="21"/>
<rectangle x1="-1.016" y1="0.381" x2="-0.889" y2="0.508" layer="21"/>
<rectangle x1="-0.762" y1="0.635" x2="-0.635" y2="0.762" layer="21"/>
<rectangle x1="-0.508" y1="0.889" x2="-0.381" y2="1.016" layer="21"/>
<rectangle x1="-0.254" y1="1.143" x2="-0.127" y2="1.27" layer="21"/>
<rectangle x1="0.127" y1="1.143" x2="0.254" y2="1.27" layer="21"/>
<rectangle x1="0.381" y1="0.889" x2="0.508" y2="1.016" layer="21"/>
<rectangle x1="0.635" y1="0.635" x2="0.762" y2="0.762" layer="21"/>
<rectangle x1="0.889" y1="0.254" x2="1.143" y2="0.381" layer="21"/>
<rectangle x1="0.889" y1="0.381" x2="1.016" y2="0.508" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="DIL24">
<wire x1="-5.08" y1="16.51" x2="-5.08" y2="-13.97" width="0.254" layer="94"/>
<wire x1="-5.08" y1="-13.97" x2="5.08" y2="-13.97" width="0.254" layer="94"/>
<wire x1="5.08" y1="-13.97" x2="5.08" y2="16.51" width="0.254" layer="94"/>
<wire x1="5.08" y1="16.51" x2="2.54" y2="16.51" width="0.254" layer="94"/>
<wire x1="-5.08" y1="16.51" x2="-2.54" y2="16.51" width="0.254" layer="94"/>
<wire x1="-2.54" y1="16.51" x2="2.54" y2="16.51" width="0.254" layer="94" curve="180"/>
<text x="-4.445" y="17.145" size="1.778" layer="95">&gt;NAME</text>
<text x="-4.445" y="-16.51" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="-7.62" y="15.24" visible="pad" length="short" direction="pas"/>
<pin name="2" x="-7.62" y="12.7" visible="pad" length="short" direction="pas"/>
<pin name="3" x="-7.62" y="10.16" visible="pad" length="short" direction="pas"/>
<pin name="4" x="-7.62" y="7.62" visible="pad" length="short" direction="pas"/>
<pin name="5" x="-7.62" y="5.08" visible="pad" length="short" direction="pas"/>
<pin name="6" x="-7.62" y="2.54" visible="pad" length="short" direction="pas"/>
<pin name="7" x="-7.62" y="0" visible="pad" length="short" direction="pas"/>
<pin name="8" x="-7.62" y="-2.54" visible="pad" length="short" direction="pas"/>
<pin name="9" x="-7.62" y="-5.08" visible="pad" length="short" direction="pas"/>
<pin name="10" x="-7.62" y="-7.62" visible="pad" length="short" direction="pas"/>
<pin name="11" x="-7.62" y="-10.16" visible="pad" length="short" direction="pas"/>
<pin name="12" x="-7.62" y="-12.7" visible="pad" length="short" direction="pas"/>
<pin name="13" x="7.62" y="-12.7" visible="pad" length="short" direction="pas" rot="R180"/>
<pin name="14" x="7.62" y="-10.16" visible="pad" length="short" direction="pas" rot="R180"/>
<pin name="15" x="7.62" y="-7.62" visible="pad" length="short" direction="pas" rot="R180"/>
<pin name="16" x="7.62" y="-5.08" visible="pad" length="short" direction="pas" rot="R180"/>
<pin name="17" x="7.62" y="-2.54" visible="pad" length="short" direction="pas" rot="R180"/>
<pin name="18" x="7.62" y="0" visible="pad" length="short" direction="pas" rot="R180"/>
<pin name="19" x="7.62" y="2.54" visible="pad" length="short" direction="pas" rot="R180"/>
<pin name="20" x="7.62" y="5.08" visible="pad" length="short" direction="pas" rot="R180"/>
<pin name="21" x="7.62" y="7.62" visible="pad" length="short" direction="pas" rot="R180"/>
<pin name="22" x="7.62" y="10.16" visible="pad" length="short" direction="pas" rot="R180"/>
<pin name="23" x="7.62" y="12.7" visible="pad" length="short" direction="pas" rot="R180"/>
<pin name="24" x="7.62" y="15.24" visible="pad" length="short" direction="pas" rot="R180"/>
</symbol>
<symbol name="DIL20">
<wire x1="-5.08" y1="11.43" x2="-5.08" y2="-13.97" width="0.254" layer="94"/>
<wire x1="-5.08" y1="-13.97" x2="5.08" y2="-13.97" width="0.254" layer="94"/>
<wire x1="5.08" y1="-13.97" x2="5.08" y2="11.43" width="0.254" layer="94"/>
<wire x1="5.08" y1="11.43" x2="2.54" y2="11.43" width="0.254" layer="94"/>
<wire x1="-5.08" y1="11.43" x2="-2.54" y2="11.43" width="0.254" layer="94"/>
<wire x1="-2.54" y1="11.43" x2="2.54" y2="11.43" width="0.254" layer="94" curve="180"/>
<text x="-4.445" y="12.065" size="1.778" layer="95">&gt;NAME</text>
<text x="-4.445" y="-16.51" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="-7.62" y="10.16" visible="pad" length="short" direction="pas"/>
<pin name="2" x="-7.62" y="7.62" visible="pad" length="short" direction="pas"/>
<pin name="3" x="-7.62" y="5.08" visible="pad" length="short" direction="pas"/>
<pin name="4" x="-7.62" y="2.54" visible="pad" length="short" direction="pas"/>
<pin name="5" x="-7.62" y="0" visible="pad" length="short" direction="pas"/>
<pin name="6" x="-7.62" y="-2.54" visible="pad" length="short" direction="pas"/>
<pin name="7" x="-7.62" y="-5.08" visible="pad" length="short" direction="pas"/>
<pin name="8" x="-7.62" y="-7.62" visible="pad" length="short" direction="pas"/>
<pin name="9" x="-7.62" y="-10.16" visible="pad" length="short" direction="pas"/>
<pin name="10" x="-7.62" y="-12.7" visible="pad" length="short" direction="pas"/>
<pin name="11" x="7.62" y="-12.7" visible="pad" length="short" direction="pas" rot="R180"/>
<pin name="12" x="7.62" y="-10.16" visible="pad" length="short" direction="pas" rot="R180"/>
<pin name="13" x="7.62" y="-7.62" visible="pad" length="short" direction="pas" rot="R180"/>
<pin name="14" x="7.62" y="-5.08" visible="pad" length="short" direction="pas" rot="R180"/>
<pin name="15" x="7.62" y="-2.54" visible="pad" length="short" direction="pas" rot="R180"/>
<pin name="16" x="7.62" y="0" visible="pad" length="short" direction="pas" rot="R180"/>
<pin name="17" x="7.62" y="2.54" visible="pad" length="short" direction="pas" rot="R180"/>
<pin name="18" x="7.62" y="5.08" visible="pad" length="short" direction="pas" rot="R180"/>
<pin name="19" x="7.62" y="7.62" visible="pad" length="short" direction="pas" rot="R180"/>
<pin name="20" x="7.62" y="10.16" visible="pad" length="short" direction="pas" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="DIL24" prefix="IC" uservalue="yes">
<description>&lt;b&gt;Dual In Line&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="DIL24" x="0" y="0"/>
</gates>
<devices>
<device name="-3" package="DIL24-3">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="11" pad="11"/>
<connect gate="G$1" pin="12" pad="12"/>
<connect gate="G$1" pin="13" pad="13"/>
<connect gate="G$1" pin="14" pad="14"/>
<connect gate="G$1" pin="15" pad="15"/>
<connect gate="G$1" pin="16" pad="16"/>
<connect gate="G$1" pin="17" pad="17"/>
<connect gate="G$1" pin="18" pad="18"/>
<connect gate="G$1" pin="19" pad="19"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="20" pad="20"/>
<connect gate="G$1" pin="21" pad="21"/>
<connect gate="G$1" pin="22" pad="22"/>
<connect gate="G$1" pin="23" pad="23"/>
<connect gate="G$1" pin="24" pad="24"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="9" pad="9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-4" package="DIL24-4">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="11" pad="11"/>
<connect gate="G$1" pin="12" pad="12"/>
<connect gate="G$1" pin="13" pad="13"/>
<connect gate="G$1" pin="14" pad="14"/>
<connect gate="G$1" pin="15" pad="15"/>
<connect gate="G$1" pin="16" pad="16"/>
<connect gate="G$1" pin="17" pad="17"/>
<connect gate="G$1" pin="18" pad="18"/>
<connect gate="G$1" pin="19" pad="19"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="20" pad="20"/>
<connect gate="G$1" pin="21" pad="21"/>
<connect gate="G$1" pin="22" pad="22"/>
<connect gate="G$1" pin="23" pad="23"/>
<connect gate="G$1" pin="24" pad="24"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="9" pad="9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-6" package="DIL24-6">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="11" pad="11"/>
<connect gate="G$1" pin="12" pad="12"/>
<connect gate="G$1" pin="13" pad="13"/>
<connect gate="G$1" pin="14" pad="14"/>
<connect gate="G$1" pin="15" pad="15"/>
<connect gate="G$1" pin="16" pad="16"/>
<connect gate="G$1" pin="17" pad="17"/>
<connect gate="G$1" pin="18" pad="18"/>
<connect gate="G$1" pin="19" pad="19"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="20" pad="20"/>
<connect gate="G$1" pin="21" pad="21"/>
<connect gate="G$1" pin="22" pad="22"/>
<connect gate="G$1" pin="23" pad="23"/>
<connect gate="G$1" pin="24" pad="24"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="9" pad="9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-9" package="DIL24-9">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="11" pad="11"/>
<connect gate="G$1" pin="12" pad="12"/>
<connect gate="G$1" pin="13" pad="13"/>
<connect gate="G$1" pin="14" pad="14"/>
<connect gate="G$1" pin="15" pad="15"/>
<connect gate="G$1" pin="16" pad="16"/>
<connect gate="G$1" pin="17" pad="17"/>
<connect gate="G$1" pin="18" pad="18"/>
<connect gate="G$1" pin="19" pad="19"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="20" pad="20"/>
<connect gate="G$1" pin="21" pad="21"/>
<connect gate="G$1" pin="22" pad="22"/>
<connect gate="G$1" pin="23" pad="23"/>
<connect gate="G$1" pin="24" pad="24"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="9" pad="9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PLLC28_P" package="PLCC28">
<connects>
<connect gate="G$1" pin="1" pad="2"/>
<connect gate="G$1" pin="10" pad="12"/>
<connect gate="G$1" pin="11" pad="13"/>
<connect gate="G$1" pin="12" pad="14"/>
<connect gate="G$1" pin="13" pad="16"/>
<connect gate="G$1" pin="14" pad="17"/>
<connect gate="G$1" pin="15" pad="18"/>
<connect gate="G$1" pin="16" pad="19"/>
<connect gate="G$1" pin="17" pad="20"/>
<connect gate="G$1" pin="18" pad="21"/>
<connect gate="G$1" pin="19" pad="23"/>
<connect gate="G$1" pin="2" pad="3"/>
<connect gate="G$1" pin="20" pad="24"/>
<connect gate="G$1" pin="21" pad="25"/>
<connect gate="G$1" pin="22" pad="26"/>
<connect gate="G$1" pin="23" pad="27"/>
<connect gate="G$1" pin="24" pad="28"/>
<connect gate="G$1" pin="3" pad="4"/>
<connect gate="G$1" pin="4" pad="5"/>
<connect gate="G$1" pin="5" pad="6"/>
<connect gate="G$1" pin="6" pad="7"/>
<connect gate="G$1" pin="7" pad="9"/>
<connect gate="G$1" pin="8" pad="10"/>
<connect gate="G$1" pin="9" pad="11"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PLLC28_S" package="S28">
<connects>
<connect gate="G$1" pin="1" pad="2"/>
<connect gate="G$1" pin="10" pad="12"/>
<connect gate="G$1" pin="11" pad="13"/>
<connect gate="G$1" pin="12" pad="14"/>
<connect gate="G$1" pin="13" pad="16"/>
<connect gate="G$1" pin="14" pad="17"/>
<connect gate="G$1" pin="15" pad="18"/>
<connect gate="G$1" pin="16" pad="19"/>
<connect gate="G$1" pin="17" pad="20"/>
<connect gate="G$1" pin="18" pad="21"/>
<connect gate="G$1" pin="19" pad="23"/>
<connect gate="G$1" pin="2" pad="3"/>
<connect gate="G$1" pin="20" pad="24"/>
<connect gate="G$1" pin="21" pad="25"/>
<connect gate="G$1" pin="22" pad="26"/>
<connect gate="G$1" pin="23" pad="27"/>
<connect gate="G$1" pin="24" pad="28"/>
<connect gate="G$1" pin="3" pad="4"/>
<connect gate="G$1" pin="4" pad="5"/>
<connect gate="G$1" pin="5" pad="6"/>
<connect gate="G$1" pin="6" pad="7"/>
<connect gate="G$1" pin="7" pad="9"/>
<connect gate="G$1" pin="8" pad="10"/>
<connect gate="G$1" pin="9" pad="11"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="DIL20" prefix="IC" uservalue="yes">
<description>&lt;b&gt;Dual In Line / Socket&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="DIL20" x="0" y="0"/>
</gates>
<devices>
<device name="" package="DIL20">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="11" pad="11"/>
<connect gate="G$1" pin="12" pad="12"/>
<connect gate="G$1" pin="13" pad="13"/>
<connect gate="G$1" pin="14" pad="14"/>
<connect gate="G$1" pin="15" pad="15"/>
<connect gate="G$1" pin="16" pad="16"/>
<connect gate="G$1" pin="17" pad="17"/>
<connect gate="G$1" pin="18" pad="18"/>
<connect gate="G$1" pin="19" pad="19"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="20" pad="20"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="9" pad="9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="S" package="SOCKET-20">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="11" pad="11"/>
<connect gate="G$1" pin="12" pad="12"/>
<connect gate="G$1" pin="13" pad="13"/>
<connect gate="G$1" pin="14" pad="14"/>
<connect gate="G$1" pin="15" pad="15"/>
<connect gate="G$1" pin="16" pad="16"/>
<connect gate="G$1" pin="17" pad="17"/>
<connect gate="G$1" pin="18" pad="18"/>
<connect gate="G$1" pin="19" pad="19"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="20" pad="20"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="9" pad="9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PLLC_P" package="PLCC20">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="11" pad="11"/>
<connect gate="G$1" pin="12" pad="12"/>
<connect gate="G$1" pin="13" pad="13"/>
<connect gate="G$1" pin="14" pad="14"/>
<connect gate="G$1" pin="15" pad="15"/>
<connect gate="G$1" pin="16" pad="16"/>
<connect gate="G$1" pin="17" pad="17"/>
<connect gate="G$1" pin="18" pad="18"/>
<connect gate="G$1" pin="19" pad="19"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="20" pad="20"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="9" pad="9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PLCC_S" package="S20">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="11" pad="11"/>
<connect gate="G$1" pin="12" pad="12"/>
<connect gate="G$1" pin="13" pad="13"/>
<connect gate="G$1" pin="14" pad="14"/>
<connect gate="G$1" pin="15" pad="15"/>
<connect gate="G$1" pin="16" pad="16"/>
<connect gate="G$1" pin="17" pad="17"/>
<connect gate="G$1" pin="18" pad="18"/>
<connect gate="G$1" pin="19" pad="19"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="20" pad="20"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="9" pad="9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="IC1" library="ic-package" deviceset="DIL24" device="-3"/>
<part name="IC2" library="ic-package" deviceset="DIL24" device="-6"/>
<part name="IC3" library="ic-package" deviceset="DIL24" device="PLLC28_S"/>
<part name="IC4" library="ic-package" deviceset="DIL20" device="PLCC_S"/>
</parts>
<sheets>
<sheet>
<plain>
</plain>
<instances>
<instance part="IC1" gate="G$1" x="25.4" y="20.32"/>
<instance part="IC2" gate="G$1" x="66.04" y="25.4"/>
<instance part="IC3" gate="G$1" x="45.72" y="22.86"/>
<instance part="IC4" gate="G$1" x="86.36" y="33.02"/>
</instances>
<busses>
</busses>
<nets>
<net name="N$1" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="1"/>
<pinref part="IC2" gate="G$1" pin="1"/>
<wire x1="58.42" y1="40.64" x2="38.1" y2="38.1" width="0.1524" layer="91"/>
<pinref part="IC3" gate="G$1" pin="1"/>
<junction x="38.1" y="38.1"/>
<wire x1="38.1" y1="38.1" x2="17.78" y2="35.56" width="0.1524" layer="91"/>
<pinref part="IC4" gate="G$1" pin="1"/>
<junction x="58.42" y="40.64"/>
<wire x1="58.42" y1="40.64" x2="78.74" y2="43.18" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$2" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="2"/>
<pinref part="IC2" gate="G$1" pin="2"/>
<wire x1="58.42" y1="38.1" x2="38.1" y2="35.56" width="0.1524" layer="91"/>
<pinref part="IC3" gate="G$1" pin="2"/>
<junction x="38.1" y="35.56"/>
<wire x1="38.1" y1="35.56" x2="17.78" y2="33.02" width="0.1524" layer="91"/>
<pinref part="IC4" gate="G$1" pin="2"/>
<junction x="58.42" y="38.1"/>
<wire x1="58.42" y1="38.1" x2="78.74" y2="40.64" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$3" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="3"/>
<pinref part="IC2" gate="G$1" pin="3"/>
<wire x1="58.42" y1="35.56" x2="38.1" y2="33.02" width="0.1524" layer="91"/>
<pinref part="IC3" gate="G$1" pin="3"/>
<junction x="38.1" y="33.02"/>
<wire x1="38.1" y1="33.02" x2="17.78" y2="30.48" width="0.1524" layer="91"/>
<pinref part="IC4" gate="G$1" pin="3"/>
<junction x="58.42" y="35.56"/>
<wire x1="58.42" y1="35.56" x2="78.74" y2="38.1" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$4" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="4"/>
<pinref part="IC2" gate="G$1" pin="4"/>
<wire x1="58.42" y1="33.02" x2="38.1" y2="30.48" width="0.1524" layer="91"/>
<pinref part="IC3" gate="G$1" pin="4"/>
<junction x="38.1" y="30.48"/>
<wire x1="38.1" y1="30.48" x2="17.78" y2="27.94" width="0.1524" layer="91"/>
<pinref part="IC4" gate="G$1" pin="4"/>
<junction x="58.42" y="33.02"/>
<wire x1="58.42" y1="33.02" x2="78.74" y2="35.56" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$5" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="5"/>
<pinref part="IC2" gate="G$1" pin="5"/>
<wire x1="58.42" y1="30.48" x2="38.1" y2="27.94" width="0.1524" layer="91"/>
<pinref part="IC3" gate="G$1" pin="5"/>
<junction x="38.1" y="27.94"/>
<wire x1="38.1" y1="27.94" x2="17.78" y2="25.4" width="0.1524" layer="91"/>
<pinref part="IC4" gate="G$1" pin="5"/>
<junction x="58.42" y="30.48"/>
<wire x1="58.42" y1="30.48" x2="78.74" y2="33.02" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$6" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="6"/>
<pinref part="IC2" gate="G$1" pin="6"/>
<wire x1="58.42" y1="27.94" x2="38.1" y2="25.4" width="0.1524" layer="91"/>
<pinref part="IC3" gate="G$1" pin="6"/>
<junction x="38.1" y="25.4"/>
<wire x1="38.1" y1="25.4" x2="17.78" y2="22.86" width="0.1524" layer="91"/>
<pinref part="IC4" gate="G$1" pin="6"/>
<junction x="58.42" y="27.94"/>
<wire x1="58.42" y1="27.94" x2="78.74" y2="30.48" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$7" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="7"/>
<pinref part="IC2" gate="G$1" pin="7"/>
<wire x1="58.42" y1="25.4" x2="38.1" y2="22.86" width="0.1524" layer="91"/>
<pinref part="IC3" gate="G$1" pin="7"/>
<junction x="38.1" y="22.86"/>
<wire x1="38.1" y1="22.86" x2="17.78" y2="20.32" width="0.1524" layer="91"/>
<pinref part="IC4" gate="G$1" pin="7"/>
<junction x="58.42" y="25.4"/>
<wire x1="58.42" y1="25.4" x2="78.74" y2="27.94" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$8" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="8"/>
<pinref part="IC2" gate="G$1" pin="8"/>
<wire x1="58.42" y1="22.86" x2="38.1" y2="20.32" width="0.1524" layer="91"/>
<pinref part="IC3" gate="G$1" pin="8"/>
<junction x="38.1" y="20.32"/>
<wire x1="38.1" y1="20.32" x2="17.78" y2="17.78" width="0.1524" layer="91"/>
<pinref part="IC4" gate="G$1" pin="8"/>
<junction x="58.42" y="22.86"/>
<wire x1="58.42" y1="22.86" x2="78.74" y2="25.4" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$9" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="9"/>
<pinref part="IC2" gate="G$1" pin="9"/>
<wire x1="58.42" y1="20.32" x2="38.1" y2="17.78" width="0.1524" layer="91"/>
<pinref part="IC3" gate="G$1" pin="9"/>
<junction x="38.1" y="17.78"/>
<wire x1="38.1" y1="17.78" x2="17.78" y2="15.24" width="0.1524" layer="91"/>
<pinref part="IC4" gate="G$1" pin="9"/>
<junction x="58.42" y="20.32"/>
<wire x1="58.42" y1="20.32" x2="78.74" y2="22.86" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$10" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="10"/>
<pinref part="IC2" gate="G$1" pin="10"/>
<wire x1="58.42" y1="17.78" x2="38.1" y2="15.24" width="0.1524" layer="91"/>
<pinref part="IC3" gate="G$1" pin="10"/>
<junction x="38.1" y="15.24"/>
<wire x1="38.1" y1="15.24" x2="17.78" y2="12.7" width="0.1524" layer="91"/>
<pinref part="IC4" gate="G$1" pin="10"/>
<junction x="58.42" y="17.78"/>
<wire x1="58.42" y1="17.78" x2="78.74" y2="20.32" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$11" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="11"/>
<pinref part="IC2" gate="G$1" pin="11"/>
<wire x1="58.42" y1="15.24" x2="38.1" y2="12.7" width="0.1524" layer="91"/>
<pinref part="IC3" gate="G$1" pin="11"/>
<junction x="38.1" y="12.7"/>
<wire x1="38.1" y1="12.7" x2="17.78" y2="10.16" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$12" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="12"/>
<pinref part="IC2" gate="G$1" pin="12"/>
<wire x1="58.42" y1="12.7" x2="38.1" y2="10.16" width="0.1524" layer="91"/>
<pinref part="IC3" gate="G$1" pin="12"/>
<junction x="38.1" y="10.16"/>
<wire x1="38.1" y1="10.16" x2="17.78" y2="7.62" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$13" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="13"/>
<pinref part="IC2" gate="G$1" pin="13"/>
<wire x1="73.66" y1="12.7" x2="53.34" y2="10.16" width="0.1524" layer="91"/>
<pinref part="IC3" gate="G$1" pin="13"/>
<junction x="53.34" y="10.16"/>
<wire x1="53.34" y1="10.16" x2="33.02" y2="7.62" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$14" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="14"/>
<pinref part="IC2" gate="G$1" pin="14"/>
<wire x1="73.66" y1="15.24" x2="53.34" y2="12.7" width="0.1524" layer="91"/>
<pinref part="IC3" gate="G$1" pin="14"/>
<junction x="53.34" y="12.7"/>
<wire x1="53.34" y1="12.7" x2="33.02" y2="10.16" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$15" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="15"/>
<pinref part="IC2" gate="G$1" pin="15"/>
<wire x1="73.66" y1="17.78" x2="53.34" y2="15.24" width="0.1524" layer="91"/>
<pinref part="IC3" gate="G$1" pin="15"/>
<junction x="53.34" y="15.24"/>
<wire x1="53.34" y1="15.24" x2="33.02" y2="12.7" width="0.1524" layer="91"/>
<pinref part="IC4" gate="G$1" pin="11"/>
<junction x="73.66" y="17.78"/>
<wire x1="73.66" y1="17.78" x2="93.98" y2="20.32" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$16" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="16"/>
<pinref part="IC2" gate="G$1" pin="16"/>
<wire x1="73.66" y1="20.32" x2="53.34" y2="17.78" width="0.1524" layer="91"/>
<pinref part="IC3" gate="G$1" pin="16"/>
<junction x="53.34" y="17.78"/>
<wire x1="53.34" y1="17.78" x2="33.02" y2="15.24" width="0.1524" layer="91"/>
<pinref part="IC4" gate="G$1" pin="12"/>
<junction x="73.66" y="20.32"/>
<wire x1="73.66" y1="20.32" x2="93.98" y2="22.86" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$17" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="17"/>
<pinref part="IC2" gate="G$1" pin="17"/>
<wire x1="73.66" y1="22.86" x2="53.34" y2="20.32" width="0.1524" layer="91"/>
<pinref part="IC3" gate="G$1" pin="17"/>
<junction x="53.34" y="20.32"/>
<wire x1="53.34" y1="20.32" x2="33.02" y2="17.78" width="0.1524" layer="91"/>
<pinref part="IC4" gate="G$1" pin="13"/>
<junction x="73.66" y="22.86"/>
<wire x1="73.66" y1="22.86" x2="93.98" y2="25.4" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$18" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="18"/>
<pinref part="IC2" gate="G$1" pin="18"/>
<wire x1="73.66" y1="25.4" x2="53.34" y2="22.86" width="0.1524" layer="91"/>
<pinref part="IC3" gate="G$1" pin="18"/>
<junction x="53.34" y="22.86"/>
<wire x1="53.34" y1="22.86" x2="33.02" y2="20.32" width="0.1524" layer="91"/>
<pinref part="IC4" gate="G$1" pin="14"/>
<junction x="73.66" y="25.4"/>
<wire x1="73.66" y1="25.4" x2="93.98" y2="27.94" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$19" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="19"/>
<pinref part="IC2" gate="G$1" pin="19"/>
<wire x1="73.66" y1="27.94" x2="53.34" y2="25.4" width="0.1524" layer="91"/>
<pinref part="IC3" gate="G$1" pin="19"/>
<junction x="53.34" y="25.4"/>
<wire x1="53.34" y1="25.4" x2="33.02" y2="22.86" width="0.1524" layer="91"/>
<pinref part="IC4" gate="G$1" pin="15"/>
<junction x="73.66" y="27.94"/>
<wire x1="73.66" y1="27.94" x2="93.98" y2="30.48" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$20" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="20"/>
<pinref part="IC2" gate="G$1" pin="20"/>
<wire x1="73.66" y1="30.48" x2="53.34" y2="27.94" width="0.1524" layer="91"/>
<pinref part="IC3" gate="G$1" pin="20"/>
<junction x="53.34" y="27.94"/>
<wire x1="53.34" y1="27.94" x2="33.02" y2="25.4" width="0.1524" layer="91"/>
<pinref part="IC4" gate="G$1" pin="16"/>
<junction x="73.66" y="30.48"/>
<wire x1="73.66" y1="30.48" x2="93.98" y2="33.02" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$21" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="21"/>
<pinref part="IC2" gate="G$1" pin="21"/>
<wire x1="73.66" y1="33.02" x2="53.34" y2="30.48" width="0.1524" layer="91"/>
<pinref part="IC3" gate="G$1" pin="21"/>
<junction x="53.34" y="30.48"/>
<wire x1="53.34" y1="30.48" x2="33.02" y2="27.94" width="0.1524" layer="91"/>
<pinref part="IC4" gate="G$1" pin="17"/>
<junction x="73.66" y="33.02"/>
<wire x1="73.66" y1="33.02" x2="93.98" y2="35.56" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$22" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="22"/>
<pinref part="IC2" gate="G$1" pin="22"/>
<wire x1="73.66" y1="35.56" x2="53.34" y2="33.02" width="0.1524" layer="91"/>
<pinref part="IC3" gate="G$1" pin="22"/>
<junction x="53.34" y="33.02"/>
<wire x1="53.34" y1="33.02" x2="33.02" y2="30.48" width="0.1524" layer="91"/>
<pinref part="IC4" gate="G$1" pin="18"/>
<junction x="73.66" y="35.56"/>
<wire x1="73.66" y1="35.56" x2="93.98" y2="38.1" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$23" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="23"/>
<pinref part="IC2" gate="G$1" pin="23"/>
<wire x1="73.66" y1="38.1" x2="53.34" y2="35.56" width="0.1524" layer="91"/>
<pinref part="IC3" gate="G$1" pin="23"/>
<junction x="53.34" y="35.56"/>
<wire x1="53.34" y1="35.56" x2="33.02" y2="33.02" width="0.1524" layer="91"/>
<pinref part="IC4" gate="G$1" pin="19"/>
<junction x="73.66" y="38.1"/>
<wire x1="73.66" y1="38.1" x2="93.98" y2="40.64" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$24" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="24"/>
<pinref part="IC2" gate="G$1" pin="24"/>
<wire x1="73.66" y1="40.64" x2="53.34" y2="38.1" width="0.1524" layer="91"/>
<pinref part="IC3" gate="G$1" pin="24"/>
<junction x="53.34" y="38.1"/>
<wire x1="53.34" y1="38.1" x2="33.02" y2="35.56" width="0.1524" layer="91"/>
<pinref part="IC4" gate="G$1" pin="20"/>
<junction x="73.66" y="40.64"/>
<wire x1="73.66" y1="40.64" x2="93.98" y2="43.18" width="0.1524" layer="91"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
</eagle>
