import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.nio.file.Paths;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.border.TitledBorder;
import javax.swing.text.DefaultCaret;

import jssc.SerialPort;
import jssc.SerialPortEvent;
import jssc.SerialPortEventListener;
import jssc.SerialPortException;
import jssc.SerialPortList;

public class Galdurino implements KeyListener, SerialPortEventListener {


	JFrame contentPane;
	JTextArea TextBereich;
	private JTextField potiVal;
	private JTextField jedFile;
	JLabel lblVolt;
	JTextArea lblGALName;
	JButton btnSaveJED;

	String portName = "/dev/ttyACM0";
	String command = "";
	int textDimension_x = 140;
	int textDimension_y = 25;
	int lastGALTag=0;
	boolean SerialPortOpenedFlag = false;
	GALHandler gal = null;
	static SerialPort serialPort;
	int potiValue =180;

	final int pxButtonHeight= 30;


	public Galdurino() {
		contentPane = new JFrame("Galdurino serial terminal");

		contentPane.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				closeSerialConnection();
				System.exit(0);
			}
		});

		// ************************ create window with text
		// **********************************
		JPanel lowerPanel = new JPanel();

		JPanel panel = new JPanel();
		contentPane.add(panel, BorderLayout.CENTER);
		GridBagLayout gbl_panel = new GridBagLayout();
		gbl_panel.columnWidths = new int[]{0, 0, 0, 0};
		gbl_panel.rowHeights = new int[]{0, 0, 0, 0, 0};
		gbl_panel.columnWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		gbl_panel.rowWeights = new double[]{0.0, 0.0, 0.0, 1.0, Double.MIN_VALUE};
		panel.setLayout(gbl_panel);

		JButton btnStatus = new JButton("Status");
		btnStatus.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				command= "status\n";
				outputText(command);
				sendAndClearCommand();
			}
		});
		btnStatus.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

			}
		});
		GridBagConstraints gbc_btnStatus = new GridBagConstraints();
		gbc_btnStatus.insets = new Insets(0, 0, 5, 5);
		gbc_btnStatus.gridx = 0;
		gbc_btnStatus.gridy = 0;
		//hack to make the buttons even size
		btnStatus.setPreferredSize(new Dimension(2, pxButtonHeight));
		gbc_btnStatus.fill = GridBagConstraints.BOTH;
		gbc_btnStatus.weightx = 1.0; // fill all available space horizontally
		gbc_btnStatus.weighty = 1.0; // fill all available space vertically

		panel.add(btnStatus, gbc_btnStatus);

		JButton btnVersion = new JButton("Version");
		btnVersion.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				command= "version\n";
				outputText(command);
				sendAndClearCommand();
			}
		});
		GridBagConstraints gbc_btnVersion = new GridBagConstraints();
		gbc_btnVersion.insets = new Insets(0, 0, 5, 5);
		gbc_btnVersion.gridx = 1;
		gbc_btnVersion.gridy = 0;
		//hack to make the buttons even size
		btnVersion.setPreferredSize(new Dimension(2, pxButtonHeight));
		gbc_btnVersion.fill = GridBagConstraints.BOTH;
		gbc_btnVersion.weightx = 1.0; // fill all available space horizontally
		gbc_btnVersion.weighty = 1.0; // fill all available space vertically


		panel.add(btnVersion, gbc_btnVersion);

		JButton btnHelp = new JButton("Help");
		btnHelp.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				command= "help\n";
				outputText(command);
				sendAndClearCommand();
			}
		});
		GridBagConstraints gbc_btnHelp = new GridBagConstraints();
		gbc_btnHelp.insets = new Insets(0, 0, 5, 5);
		gbc_btnHelp.gridx = 2;
		gbc_btnHelp.gridy = 0;
		//hack to make the buttons even size
		btnHelp.setPreferredSize(new Dimension(2, pxButtonHeight));
		gbc_btnHelp.fill = GridBagConstraints.BOTH;
		gbc_btnHelp.weightx = 1.0; // fill all available space horizontally
		gbc_btnHelp.weighty = 1.0; // fill all available space vertically
		panel.add(btnHelp, gbc_btnHelp);

		JButton btnIdentify = new JButton("Identify");
		btnIdentify.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				command = String.format("identify %d\n", potiValue);
				outputText(command);
				sendAndClearCommand();
			}
		});
		GridBagConstraints gbc_btnIdentify = new GridBagConstraints();
		gbc_btnIdentify.insets = new Insets(0, 0, 5, 5);
		gbc_btnIdentify.gridx = 3;
		gbc_btnIdentify.gridy = 0;
		//hack to make the buttons even size
		btnIdentify.setPreferredSize(new Dimension(2, pxButtonHeight));
		gbc_btnIdentify.fill = GridBagConstraints.BOTH;
		gbc_btnIdentify.weightx = 1.0; // fill all available space horizontally
		gbc_btnIdentify.weighty = 1.0; // fill all available space vertically
		panel.add(btnHelp, gbc_btnHelp);
		panel.add(btnIdentify, gbc_btnIdentify);

		JButton btnRead16v8 = new JButton("Read 16V8");
		btnRead16v8.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				command = String.format("16v8read %d\n", potiValue);
				outputText(command);
				sendAndClearCommand();
			}
		});
		GridBagConstraints gbc_btnRead16v8 = new GridBagConstraints();
		gbc_btnRead16v8.insets = new Insets(0, 0, 5, 5);
		gbc_btnRead16v8.gridx = 0;
		gbc_btnRead16v8.gridy = 1;
		//hack to make the buttons even size
		btnRead16v8.setPreferredSize(new Dimension(2, pxButtonHeight));
		gbc_btnRead16v8.fill = GridBagConstraints.BOTH;
		gbc_btnRead16v8.weightx = 1.0; // fill all available space horizontally
		gbc_btnRead16v8.weighty = 1.0; // fill all available space vertically
		panel.add(btnHelp, gbc_btnHelp);
		panel.add(btnRead16v8, gbc_btnRead16v8);

		JButton btnRead20v8 = new JButton("Read 20V8");
		btnRead20v8.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				command =String.format("20v8read %d\n", potiValue);
				outputText(command);
				sendAndClearCommand();
			}
		});
		GridBagConstraints gbc_btnRead20v8 = new GridBagConstraints();
		gbc_btnRead20v8.insets = new Insets(0, 0, 5, 5);
		gbc_btnRead20v8.gridx = 1;
		gbc_btnRead20v8.gridy = 1;
		//hack to make the buttons even size
		btnRead20v8.setPreferredSize(new Dimension(2, pxButtonHeight));
		gbc_btnRead20v8.fill = GridBagConstraints.BOTH;
		gbc_btnRead20v8.weightx = 1.0; // fill all available space horizontally
		gbc_btnRead20v8.weighty = 1.0; // fill all available space vertically
		panel.add(btnRead20v8, gbc_btnRead20v8);

		JButton btnRead22V10 = new JButton("Read 22V10");
		btnRead22V10.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				command =String.format("22v10read %d\n", potiValue);
				outputText(command);
				sendAndClearCommand();
			}
		});
		GridBagConstraints gbc_btnRead22V10 = new GridBagConstraints();
		gbc_btnRead22V10.insets = new Insets(0, 0, 5, 0);
		gbc_btnRead22V10.gridx = 2;
		gbc_btnRead22V10.gridy = 1;
		//hack to make the buttons even size
		btnRead22V10.setPreferredSize(new Dimension(2, pxButtonHeight));
		gbc_btnRead22V10.fill = GridBagConstraints.BOTH;
		gbc_btnRead22V10.weightx = 1.0; // fill all available space horizontally
		gbc_btnRead22V10.weighty = 1.0; // fill all available space vertically
		panel.add(btnRead22V10, gbc_btnRead22V10);



		TitledBorder potiLabel;
		potiLabel = BorderFactory.createTitledBorder("Poti value (0-255):");

		potiVal = new JTextField();
		potiVal.setBorder(potiLabel);
		potiVal.setText(Integer.toString(potiValue));
		potiVal.addKeyListener(new KeyAdapter() {
			public void keyReleased(KeyEvent e) {
				if(!potiVal.getText().isEmpty()) {
					potiValue= Integer.parseInt(potiVal.getText());
				}
			}
		});
		GridBagConstraints gbc_potiVal = new GridBagConstraints();
		gbc_potiVal.insets = new Insets(0, 0, 5, 5);
		gbc_potiVal.fill = GridBagConstraints.HORIZONTAL;
		gbc_potiVal.gridx = 0;
		gbc_potiVal.gridy = 2;
		panel.add(potiVal, gbc_potiVal);
		potiVal.setColumns(4);


		JButton btnSetPoti = new JButton("Set poti value");
		btnSetPoti.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				command =String.format("setpoti %d\n", potiValue);
				outputText(command);
				sendAndClearCommand();
			}
		});
		GridBagConstraints gbc_btnSetPoti = new GridBagConstraints();
		gbc_btnSetPoti.insets = new Insets(0, 0, 5, 0);
		gbc_btnSetPoti.gridx = 1;
		gbc_btnSetPoti.gridy = 2;
		//hack to make the buttons even size
		btnSetPoti.setPreferredSize(new Dimension(2, pxButtonHeight));
		gbc_btnSetPoti.fill = GridBagConstraints.BOTH;
		gbc_btnSetPoti.weightx = 1.0; // fill all available space horizontally
		gbc_btnSetPoti.weighty = 1.0; // fill all available space vertically
		panel.add(btnSetPoti, gbc_btnSetPoti);

		lblVolt = new JLabel("VPP: 12.0 V");
		GridBagConstraints gbc_lblVolt_1 = new GridBagConstraints();
		gbc_lblVolt_1.insets = new Insets(0, 0, 5, 0);
		gbc_lblVolt_1.gridx = 2;
		gbc_lblVolt_1.gridy = 2;
		panel.add(lblVolt, gbc_lblVolt_1);

		lblGALName = new JTextArea("GAL: UNKNOWN"+System.getProperty("line.separator")+"VENDOR: UNKNOWN");
		lblGALName.setLineWrap(true);
		lblGALName.setWrapStyleWord(true);
		lblGALName.setEditable(false);
		GridBagConstraints gbc_lblGALName = new GridBagConstraints();
		gbc_lblGALName.insets = new Insets(0, 0, 5, 0);
		gbc_lblGALName.gridx = 3;
		gbc_lblGALName.gridy = 1;
		gbc_lblGALName.gridheight = 2;
		//hack to make the buttons even size
		lblGALName.setPreferredSize(new Dimension(2, pxButtonHeight));
		gbc_lblGALName.fill = GridBagConstraints.BOTH;
		gbc_lblGALName.weightx = 1.0; // fill all available space horizontally
		gbc_lblGALName.weighty = 1.0; // fill all available space vertically
		panel.add(lblGALName, gbc_lblGALName);


		TitledBorder fileNameLabel;
		fileNameLabel = BorderFactory.createTitledBorder("JED-File:");

		jedFile = new JTextField();
		jedFile.setText("test.jed");
		jedFile.setBorder(fileNameLabel);
		GridBagConstraints gbc_jedFile = new GridBagConstraints();
		gbc_jedFile.insets = new Insets(0, 0, 5, 5);
		gbc_jedFile.fill = GridBagConstraints.HORIZONTAL;
		gbc_jedFile.gridx = 0;
		gbc_jedFile.gridy = 3;
		gbc_jedFile.gridwidth = 2;
		panel.add(jedFile, gbc_jedFile);
		jedFile.setColumns(40);

		JButton btnBrowseJED = new JButton("Browse");
		btnBrowseJED.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JFileChooser fileChooser = new JFileChooser();
				fileChooser.setDialogTitle("Specify a file to save");
				fileChooser.setCurrentDirectory(new File(Paths.get("").toAbsolutePath().toString()));

				int userSelection = fileChooser.showSaveDialog(contentPane);

				if (userSelection == JFileChooser.APPROVE_OPTION) {
					jedFile.setText(fileChooser.getSelectedFile().getAbsolutePath());
				}
			}

		});
		GridBagConstraints gbc_btnBrowseJED = new GridBagConstraints();
		gbc_btnBrowseJED.gridx = 2;
		gbc_btnBrowseJED.gridy = 3;

		//hack to make the buttons even size
		btnBrowseJED.setPreferredSize(new Dimension(2, pxButtonHeight));
		gbc_btnBrowseJED.fill = GridBagConstraints.BOTH;
		gbc_btnBrowseJED.weightx = 1.0; // fill all available space horizontally
		gbc_btnBrowseJED.weighty = 1.0; // fill all available space vertically

		panel.add(btnBrowseJED, gbc_btnBrowseJED);

		btnSaveJED = new JButton("Save JED");
		btnSaveJED.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(!gal.name.equals("UNKNOWN")){
					JEDFile jed = new JEDFile(gal);
					jed.saveJED(jedFile.getText());
					outputText("Saved file: "+jedFile.getText()+"\r\n");
				}
			}
		});
		btnSaveJED.setEnabled(false);
		GridBagConstraints gbc_btnSaveJED = new GridBagConstraints();
		gbc_btnSaveJED.gridx = 3;
		gbc_btnSaveJED.gridy = 3;

		//hack to make the buttons even size
		btnSaveJED.setPreferredSize(new Dimension(2, pxButtonHeight));
		gbc_btnSaveJED.fill = GridBagConstraints.BOTH;
		gbc_btnSaveJED.weightx = 1.0; // fill all available space horizontally
		gbc_btnSaveJED.weighty = 1.0; // fill all available space vertically
		panel.add(btnSaveJED, gbc_btnSaveJED);

		contentPane.getContentPane().add(lowerPanel, "South");

		TextBereich = new JTextArea(textDimension_y, textDimension_x);
		TextBereich.setForeground(Color.GREEN);
		TextBereich.setBackground(Color.BLACK);

		TextBereich.setLineWrap(true);
		TextBereich.setWrapStyleWord(true);
		TextBereich.setEditable(false);
		TextBereich.setCaretColor(Color.GREEN);
		lowerPanel.add(new JScrollPane(TextBereich));
		DefaultCaret caret = (DefaultCaret) TextBereich.getCaret();
		caret.setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE);
		caret.setBlinkRate(500);
		caret.setVisible(true);

		TextBereich.addKeyListener(this);

		contentPane.pack();
		contentPane.setVisible(true);


		// ************************ show serial ports
		// ****************************************

		String[] portNames = SerialPortList.getPortNames();

		// ***********************************************************************************

		if (portNames.length > 0) {
			portName = (String) JOptionPane.showInputDialog(null, "Port", "choose port", JOptionPane.QUESTION_MESSAGE,
					null, portNames, portNames[0]);

			if (portName != null) {
				openSerialConnection();

			} else
				this.outputText("no ports found, please exit\n\r");
		} else {
			this.outputText("no ports found, exit\n\r");
		}

		//this makes the cursor blink!
		this.outputText(" ");
		this.removeLastChar();
	}

	public void openSerialConnection() {
		this.outputText("trying to open port ");
		this.outputText(portName + "\r\n");

		serialPort = new SerialPort(portName);
		try {
			serialPort.openPort();
			serialPort.setParams(SerialPort.BAUDRATE_9600, SerialPort.DATABITS_8, SerialPort.STOPBITS_1,
					SerialPort.PARITY_NONE);

			// clear rx data
			serialPort.purgePort(SerialPort.PURGE_RXCLEAR);

			this.outputText("open\r\n");

			int mask = SerialPort.MASK_RXCHAR;// Prepare mask
			serialPort.setEventsMask(mask);// Set mask
			serialPort.addEventListener(this);// Add SerialPortEventListener
			SerialPortOpenedFlag = true;

		} catch (SerialPortException ex) {
			this.outputText("port error\r\n");
			System.err.println(ex);
		}
	}

	public void closeSerialConnection() {
		try {
			if (SerialPortOpenedFlag) {
				serialPort.closePort();
				SerialPortOpenedFlag = false;
			}

		} catch (SerialPortException e) {
			e.printStackTrace();
		}
	}

	int NumberOfLinesUntilTextCleared = 1000;

	private void outputText(final String rx) {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				TextBereich.append(rx);

				String text = TextBereich.getText();

				if(text.endsWith("END\r\n")) {
					parseGAL();
				}

				if(text.endsWith("V\r\n")) {
					parseVoltage();
				}

				if(text.endsWith("ID end\r\n")) {
					parseID();
				}

				if (TextBereich.getLineCount() > NumberOfLinesUntilTextCleared) {
					int lineEnd = text.indexOf("\r\n") + 1;
					text = text.substring(lineEnd);
					TextBereich.setText(text);
					TextBereich.setCaretPosition(TextBereich.getText().length());
				}
			}
		});
	}

	private void removeLastChar() {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				String text = TextBereich.getText();

				if (text.length()>0) {
					text = text.substring(0, text.length()-1);
					TextBereich.setText(text);
					DefaultCaret caret = (DefaultCaret) TextBereich.getCaret();
					caret.setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE);
					caret.setVisible(true);
				}
				DefaultCaret caret = (DefaultCaret) TextBereich.getCaret();
				caret.setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE);
				caret.setVisible(true);
			}
		});
	}

	public void serialEvent(SerialPortEvent event) {
		if (event.isRXCHAR()) {
			int numberOfBytes = event.getEventValue();
			if (numberOfBytes > 0) {
				try {
					byte buffer[] = serialPort.readBytes(numberOfBytes);
					int n;
					String rxString = "";
					for (n = 0; n < buffer.length; n++)
						rxString += ((char) (buffer[n]));

					outputText(rxString);
				} catch (SerialPortException ex) {
					System.out.println(ex);
				}
			}
		}
	}

	private void parseGAL() {
		String textBlock = this.TextBereich.getText();
		int start = textBlock.lastIndexOf("GAL:");
		if (start >= 0) {
			textBlock = textBlock.substring(start);
			this.gal = new GALHandler(textBlock);
			this.gal.printGAL();
			btnSaveJED.setEnabled(this.gal != null && !this.gal.name.equals("UNKNOWN"));
			lblGALName.setText(String.format(("GAL: %s (%02x)"+System.getProperty("line.separator")+
					 "VENDOR: %s (%02x)"), this.gal.name, this.gal.id, this.gal.vendor, this.gal.vendorID));
		}

	}

	private void parseID() {
		String textBlock = this.TextBereich.getText();
		int start = textBlock.lastIndexOf("GAL:");
		if (start >= 0) {
			textBlock = textBlock.substring(start);
			String[] tokens = textBlock.split("\r\n");
			int id=0, vendor=0;
			String name="UNKNOWN", vendorName="UNKNOWN";
			for(int i=0; i< tokens.length; ) { //do a variable jumpwidth
				switch(tokens[i]) {
				case "GAL:":
						id=Integer.decode("0x"+tokens[i+1]);
						i+=2;
						name = GALHandler.getName(id);
						break;
				case "VENDOR:":
						vendor=Integer.decode("0x"+tokens[i+1]);
						vendorName = GALHandler.getVendor(vendor);
						i+=2;
						break;
				default:
						i++;
						break;
				}
			}
			lblGALName.setText(String.format(("GAL: %s (%02x)"+System.getProperty("line.separator")+
											 "VENDOR: %s (%02x)"), name,id,vendorName,vendor));
		}
	}

	private void parseVoltage() {
		String textBlock = this.TextBereich.getText();
		int start= textBlock.lastIndexOf("VPP-voltage:");
		if (start>=0) {
			textBlock = textBlock.substring(textBlock.lastIndexOf("VPP-voltage:")+ 13);
			textBlock = textBlock.substring(0,textBlock.indexOf("V"));
			this.lblVolt.setText(String.format("VPP: %2.1f V", Double.parseDouble(textBlock)));
		}
	}

	@Override
	public void keyTyped(KeyEvent e) {
	}

	@Override
	public void keyPressed(KeyEvent e) {

			switch(e.getKeyCode()) {
			case KeyEvent.VK_ENTER:
				outputText("" + e.getKeyChar());
				this.command = this.command +e.getKeyChar();
				this.sendAndClearCommand();
				break;
			case KeyEvent.VK_BACK_SPACE:
				if(this.command.length()>0) {
					this.command = this.command.substring(0, this.command.length()-1);
					removeLastChar();
				}
				break;
			default:
				outputText("" + e.getKeyChar());
				this.command = this.command +e.getKeyChar();
				break;
			}

	}

	public void sendAndClearCommand() {
		try {
			if (SerialPortOpenedFlag) {
					serialPort.writeString(this.command);
				this.command = "";
			}
		} catch (SerialPortException e1) {
			e1.printStackTrace();
		}
	}

	@Override
	public void keyReleased(KeyEvent arg0) {
		// TODO Auto-generated method stub

	}

	public static void main(String[] args) {
		new Galdurino();
	}
}
