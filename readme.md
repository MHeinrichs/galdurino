# GALDURINO
 
This is an Arduinio-shield to read 16V8, 20V8 and 22V10 GALs. 

Supported manufactors are: 

Lattice, STMicrosystems (aka Thompson) and National Semiconductor

ATF-GALs from ATMEL are currently NOT supportet and I don't know any way how to work around read protected ATF-GALs.

AMD PALCE-GAls and classic PALs are  NOT supported, too.
**DO NOT INSERT PAL and PALCE - ICs! They will definetly break, if atempted to be read! They work in a completely different way!**

Most read protected 16V8 and 20V8 can be read with this thing due to a bug in the read protection!

![Galdurino](https://gitlab.com/MHeinrichs/galdurino/raw/master/Bilder/klein/Version%203-k.jpg)


## Things to improve
1. Proper GUI
2. JED-Export
3. Write support

**DISCLAIMER!!!!**  
**THIS IS A HOBBYIST PROJECT! I'M NOT RESPONSIBLE IF IT DOESN'T WORK OR DESTROYS YOUR VALUABLE DATA!**

## License
1. Do whatever you want with this, if you do it for private use and mention this work as origin. 
2. If you want to make money out of this work, ask me first!
3. If you copy parts of this work to your project, mention the source.
4. If you use parts of this work for closed source, burn in hell and live in fear!

## Theory of operation
You need a Arduino or a clone to run this shield!
The shield does the following:
* It has a DC-DC (MC33063)converter to provide the high VPP-voltages needed for reading (12V) and writing (up to 19V)
* The DC-DC converter is controlled by a digital SPI-controlled potentiometer-IC (MCP 4151-103E/P)
* VCC and VPP supplys are switchable via FET-Transistors
* The Arduino makes all the timings to read the GAL and transmits the data via its Serial2USB interface

## What is in this repository?
This repository consists of several directories:
* PCB: The board, schematic and bill of material (BOM) are in this directory. 
* Docs: Some documentation for JED and a copy of the anchient galbalast-project
* Galdurino-Firmware: The ino-file for the Arduino-firmware
* JED-Converter: The sources for the java-application to controll the Arduino
* Bilder: Some pictures of this project
* root directory: the readme

## How to open the PCB-files?
You find the eagle board and schematic files (brd/sch) for the pcb. The software can be [downloaded](https://www.autodesk.com/products/eagle/overview) for free after registration. KiCad can import these files too. But importers are not 100% reliable... 

## How to make a PCB?
You can generate Gerber-files from the brd files according to the specs of your PCB-manufactor. However, you can try to use the Gerber-files provided. Some specs: min trace width: 0.15mm/6mil, min trace dist: 0.15mm/6mil, min drill: 0.3mm

## How to get the parts?
Most parts can be found on digikey or mouser or a parts [supplier of your choice](https://www.reichelt.de). 
A list of parts is found in the file [Gal-Shield.bom](https://gitlab.com/MHeinrichs/galdurino/raw/master/PCB/Gal-Shield.bom)

## How to programm the Arduino board?
1. Download the latest [arduino ide](https://www.arduino.cc/en/Main/Software).
2. Connect your *EMPTY* arduino with the Galdurino-shield to your PC
3. Configure your IDE according to your arduino-board
4. Open the ino-file in the Galdurini-Firmwarefolder, click on "Upload sketch"
5. Open the serial terminal (9600 baud 81n) and enter "status" 
6. Watch the statusreport of your Galduino

## How install/open the software
1. Install JAVA on your PC (or use the java-version provided in the Arduino IDE)
2. Open a comandline in the directory where the file Galdurino.jar is located 
3. Run "javaw Galdurino.jar"
4. Choose the appropriate COM-port
5. Enjoy the best GUI ever ;).


## How to setup the Galdurino the first time
Only for the first time: You have to init the delays of your Galdurino. These values will be stored in the EEPROM of the ATMEL later on.

1. Connect your Galdurino via USB (or if you like to hack via the serial port)
2. Open the software (see above)
3. Connect to the COM-Port
4. Check the Galdurino: Click "Status"
5. The terminal should now display some stats including a line "Delays: ...." with some random stuff
6. Click in the terminal window and type "setdelays 10 0 100 0" Enter
7. Click Status again or type "status" in the terminal
8. The status should now contain the line "Delays: 10 0 100 0"
9. Done

If you want to know where these values are used, look in the source code of the Galdurino. 

## How to check if the board is well calibrated?

Do the following with NO IC inserted!

1. Open the software and make the Galdurino speak to it (Steps 1-4 in the init)
2. Set the Potivalue to 180 ( a good starting point) 
3. Click "Status". VPP should now be ~12.1V
5. Enter "VPP on"
6. Measure the voltage at the EDIT-pin (Pin 2 on the right most socket). It should read the same as the value in "status"
7. Repeat steps 2-6 with potivals 0, 255 and some values in between.
8. If the values do not match, check all resistors, faulty solterpoints and ask for help (see below) 
9. If the values match you are done!
10. Enter "VPP off" in the terminal window

## How to insert the IC

At first, identify the GAL you try to read by its label. Is it a 16V8, 20V8 or a 22V10? 

**DO NOT INSERT PAL and PALCE - ICs! They will definetly break, if atempted to be read! They work in a different way!**

Then check the orientation: Dual inline package (DIP) ICs have a notch on one side, plastic leaded chip carriers (PLCC)-ICs have a little dot on one side. Both markings indicate the side, where pin 1 is located.

Now it's time to put the adapter socket on the Galdurino. DIP-ICs can be put directly on the shield but this would increase the wear of the sockets and you risk the legs to be bended!
The shield has three sockets: One for each basic GAL-type. If the USB-Port faces upwards, the PIN 1-notch/dot faces downwards. The sockets are for: 22V10 (left), 20v8 (middle) and 16V8 (right). The adapter has 24 pins, while the 16v8 has only 20 Pins. Place the adapter in a way, that the PIN1-label on the adapter matches pin 1 of the socket. 

Here you see the adapter mounted for a 20V8:

![Insert IC](https://gitlab.com/MHeinrichs/galdurino/raw/master/Bilder/klein/V3%20mounted-k.jpg)

In case of the 16V8 four pins are "free in the air" between the socket and U3. You can see this here: 

![16v8](https://gitlab.com/MHeinrichs/galdurino/raw/master/Bilder/klein/V3%20side-k.jpg)

Now put in the GAL in the ZIF-socket or in one of the PLCC-Sockets. The pin 1-notch/dot always faces DOWN!

## How to read a GAL

1. Open the software and make the Galdurino speak to it (Steps 1-4 in the init section above)
2. Set the Potivalue to 180 ( a good starting point) 
3. Click "Status". VPP should now be ~12.1V - Don't be afraid, its NOT set to the IC yet!
4. Place the GAL-Adapter onto the shield (see section above)
5. Put in a (readprotected) GAL
6. Press "Identify"
7. Check if the text field to the right shows feasible information.
8. Press the according "Read xyz" button
9. If the fused are all "1" or one line is all "1" you have to try to read again and/or alter the potival. You do not have to press "Set Poti" every time. It's done automaticallywhen you pres "Read xyz" .
10. If the fusemap looks OK (all lines contain a lot of zeros but a sufficient amount of ones) enter a file name (or press Browse and select a file)
11. Pres "Save JED"
12. Burn a replacement GAL with the new fuse map and veryfy its functionality in the circuit.

Done!

## How to find the right poti value to read a (protected) GAL?

Every GAL has two trigger voltages on the EDIT-pin. One to switch into READ-mode and one to switch into WRITE-mode. Usually one can read the GAL putting 12V on the Edit pin.  Putting 15-22V (depending on the GAL-Type) on the EDIT-pin triggers the writing mode. 
The read-protection can be fooled, if the read-voltage value is applied before power up (5V on VCC). Sometimes it needs to be close to the READ -trigger voltage (usually down to 9 V ~ 130 Potival) or around the WRITE-trigger voltage (PotiVal 220-230). Sometimes the read-protection is already fooled just by putting 12V (180 Potival) on EDIT before power up .

So try different values around 130, 180 and 220. A good read usually has a lot of zeros in each fuse line. Lines completely filled with "1" indicate an unsuccessfull read. Lines or blocks of lines complete filled with "0" are suspicious but could be possible. If you find a "sweet spot" but some lines are "1" try to read again. Sometimes a GAL need some "attempts" to be cracked.   

## It doesn't work! How to I get help?
For support visit the [a1k-forum](https://www.a1k.org/forum/index.php?threads/73865/). Yes, you need to register and it's German, but writing in English is accepted. For bug-reports write a ticket here ;). 
